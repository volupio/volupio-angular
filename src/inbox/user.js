/**
 * @description
 * Available conversations
 * @returns {undefined}
 */
angular.module('volupio.inbox').
        directive('inboxUser', function() {
            return {
                templateUrl: 'inboxUser.html',
                scope: {
                    user: "@"
                }
            };
        });