angular.module('volupio.inbox').
service('inboxApi', ['$http', function($http) {
                this.post = function(toId, dto) {
                    return $http.post('/api/inbox/' + toId, dto);
                };

                this.postReply = function(inboxId, message) {
                	return $http.post('/api/inbox/' + inboxId + '/reply');
                };

                this.get = function(inboxId) {
                	return $http.get('/api/inbox/' + inboxId);
                };

                this.find = function(skip, take) {
                    return $http.get('/api/messages');
                };
            }]);