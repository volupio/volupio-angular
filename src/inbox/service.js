angular.module('volupio.inbox').
 factory('inboxSvc', ['$rootScope', 'inboxApi', '$q', function($rootScope, $q, api) {
                /*
                 * Inbox holder
                 * @type Array
                 */
                var results = [];

                return {
                    createConversation: function(toId, subject, content) {
                        var deferred = $q.defer();
                        var req = {
                            subject: subject,
                            content: content
                        };

                        inboxId.post(toId, req).
                        then(function(res) {
                            deferred.resolve(res.data);
                        }, function(res) {
                            deferred.reject(res);
                        });

                        return deferred.promise;
                    },
                    sendMessage: function(inboxId, message) {
                        var deferred = $q.defer();

                        inboxApi.postReply(inboxId, message).
                        then(function(res) {
                            deferred.resolve(res.data);
                        }, function(res) {
                            deferred.reject(res);
                        });

                        return deferred.promise;
                    },
                    getConversation: function(inboxId) {
                        var deferred = $q.defer();

                        inboxId.get(inboxId).
                        then(function(res) {
                            deferred.resolve(res.data);
                        }, function(res) {
                            deferred.reject(res);
                        });

                        return deferred.promise;
                    },
                    getConversations: function(skip, take) {
                        var deferred = $q.defer();

                        api.find(skip, take).then(function(res) {
                            deferred.resolve(res.data);
                        }, function(res) {
                            deferred.reject(res);
                        });

                        return deferred.promise;
                    }
                };
            }]);