angular.module('volupio.inbox', ['volupio']).
        config([function() {

            }]).
        run(['$rootScope', '$templateCache', function($rootScope, $templateCache) {


                
                var tplReply = 
                        '<div class="media inbox__reply">' +
                            '<a class="inbox__reply_avatar_wrap"><img ng-src="{avatar}" /></a>' +
                            '<div class="media-body">' +
                                '<small class="inbox__reply_time" ng-bind="when"></small>' +
                                '<h5 class="media-heading inbox__reply_heading" ng-bind="from.name"></h5>' +
                                '<p class="inbox__reply_body" ng-bind="message"></p>' +
                            '</div>' +
                        '</div>';
                $templateCache.put('inboxReply.html', tplReply);
                
                var tplUser =
                         '<div class="media conversation">' +
                            '<a class="pull-left" href="#">'  +
                                '<img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 50px; height: 50px;" ng-src="{{from.avatar}}">'  +
                            '</a>' +
                            '<div class="media-body">' +
                                '<h5 class="media-heading">Naimish Sakhpara</h5>' +
                                '<small>Hello</small>' +
                            '</div>' +
                        '</div>';
            }]);