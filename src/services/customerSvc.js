/**
 * @ngdice overview
 * @name volupio.customer
 *
 * @description
 * Customers module
 */
var customer = angular.module('volupio.customer', ['volupio']);
customer.service('customerSvc', ['Restangular', '$q', function(Restangular, $q) {
	var customerSvc = {};

	customerSvc.find = function(skip, take) {
		var defer = $q.defer(),
                    req = { 
                        skip: skip,
                        take: take
                    };
		Restangular.all('customer').customGET('', req).then(function(res) {
			defer.resolve(res);
		}, function(res) {
			defer.reject(res);
		});
		return defer.promise;
	}

	customerSvc.create = function(model) {
		var defer = $q.defer(),
			req = angular.copy(model);
                
		Restangular.all('customer').customPOST(req, '').then(function(res) {
			defer.resolve(res);
		}, function(res) {
			defer.reject(res);
		});

		return defer.promise;
	};

	/**
	 * @ngdoc function
	`* @name get customer
	 * 
	 * @description
	 * Create a new customer
	 */
	customerSvc.getCustomer = function(id) {
		var defer = $q.defer();
		Restangular.all('customer').one(id).get().then(function(res) {
			defer.resolve(res);
		}, function(res) {
			defer.reject(reject);
		});
		return defer.promise;
	};
	/**
	 * @ngdoc function
	 * @name Save customer basic information
	 *
	 * @description
	 * Updates customer basic information
	 */
	customerSvc.saveBasicInfo = function(id, name, firstName, lastName, company) {
		var defer = $q.defer(),
			req = {
				id: id,
				name: name,
				firstName: firstName,
				lastName: lastName,
                                company: company
			};

		Restangular.all('customer').one(id).customPUT(req, '').then(function(res) {
			defer.resolve(res);
		}, function(res) {
			defer.reject(res);
		});

		return defer.promise;
	};
        
        customerSvc.save = function(id, model) {
            var defer = $q.defer(),
                req = angular.copy(model);

            Restangular.all('customer').one(id).customPOST(req, 'info').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });
            
            return defer.promise;
        };
	/**
	 * @ngdoc function
	 * @name Add contact
	 *
	 * @description
	 * Add a contact to a customer
   	 * A contact consists in key and value
   	 * ie: key: 'Email', value: 'asd@asd.com', key: 'mobile', value: '2323 2332'
   	 */
	customerSvc.addContact = function(id, contactKey, contactValue) {
		var req = {
			id: id,
			contactKey: contactKey,
			contactValue: contactValue
		};
		Restangular.all('customer').one(id).one('contact').customPOST(req, '').then(function(res) {
			defer.resolve(res);
		}, function(res) {
			defer.reject(res);
		});
	};
        
        customerSvc.remove = function(customerId) {
          var deferred = $q.defer();
          Restangular.all('customer').one(customerId).remove().then(function(res) {
              deferred.resolve(res);
          }, function(res){
              deferred.reject(res);
          })
          return deferred.promise;
        };

	
	return customerSvc;
}]);