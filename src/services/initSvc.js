/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('volupio').service('initSvc', ['Restangular', '$q', function(Restangular, $q) {
    var svc = {};
    
    svc.init = function(){
        var deferred = $q.defer();
        
        Restangular.one('init').customGET('', {})
                .then(function(res) {
                    deferred.resolve(res.originalResponse);
                }, function(res) {
                    deferred.reject(res);
                });
        
        return deferred.promise;
    };
        
    return svc;
}]);