/**
 * @ngdoc overview
 * @name FileUploader Extensions
 * 
 * @description
 * Extensions for angular-file-uploader module
 * Most of them aren't really common utils but i use them with volupio based apps
 * 
 * @author Guilherme Cardoso <email@guilhermecardoso.pt>
 * @copyright Volupio <info@volupio.com>
 * @license This software is reserved to volupio only 
 */
angular.
        module('volupio').
        factory('$fileUploaderExt', ['$q',
            function($q) {
                return {
                    convertFiles: function(files) {
                        var list = [];
                        angular.forEach(files, function(obj, index) {
                            list.push({
                                file: {
                                    name: obj.name,
                                    /* Unfortunately i was stupid and i named this property a reserved word 
                                     * Access length from array instead. Not sure when i'll fix this, a deserilization in api responses or change the domain
                                     * */
                                    size: obj['length'] 
                                },
                                progress: 100,
                                isUploaded: true,
                                isSuccess: true
                            });
                        });
                    }
                };
            }]);