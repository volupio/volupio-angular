angular.module('volupio').
	service('fileApi', ['$q', '$http', function($q, $http) {
		
		this.get = function(id) {
			return $http.get('/api/file/' + id);
		};

		this.remove = function(id){
			return $http({
				method: 'DELETE',
				url: '/api/file'
			});;
		};

		this.put = function(id, model) {
			return $http.post('/api/file/' + id, model);
		}
	}]).
	factory('fileEditSvc', ['fileApi', '$q', 'modalSvc', function(fileApi, $q, modalSvc) {
		return {
			save: function(id, name, content) {
				var deferred = $q.defer();
				
				fileApi.put(id, {
					name: name,
					content: content
				}).then(function(res) {
					deferred.resolve(res.data);
					modalSvc.display(res);
				}, function(res) {
					deferred.reject(res);
					modalSvc.display(res);
				});

				return deferred.promise;
			},
			getCtrl: function(id) {
				var deferred = $q.defer();
				
				fileApi.get(id).then(function(res) {
					deferred.resolve({
						file: res.data.result
					});
				});

				return deferred.promise;
			}
		};
	}]).
	controller('fileEditCtrl', ['$scope', 'fileEditSvc', 'fileId', '$modalInstance', function($scope, fileEditSvc, fileId, $modalInstance) {
		
		$scope.file = {};

		fileEditSvc.getCtrl(fileId).then(function(res) {
			$scope.file.name = res.file.name;
			$scope.file.content = res.file.content;
		});

		$scope.save = function(){
			fileEditSvc.save(fileId, $scope.file.name, $scope.file.content).
			then(function(res) {
				$modalInstance.close(res);
			}, function(res) {
				$modalInstance.dismiss(res);
			});
		};

		$scope.cancel = function(){
			$modalInstance.dismiss();
		};

	}]);