(function(){
    var svcFn = function($http, $q, $rootScope, $timeout){

        var fn = function(initModel){
            this.unread = _.isUndefined(initModel) || _.isNull(initModel) || !_.isArray(initModel.unread) ? [] : initModel.unread;
            this.pending = false;
        };

        fn.prototype.read = function(openeded) {
            if(!openeded) {
                if(this.pending) {
                    this.unread = [];
                }
                return false;
            }
            var self = this,
                deferred = $q.defer(),
                successFn = function(res) {
                    $timeout(function(){
                        self.pending = true;
                    }, 2000);
                    deferred.resolve(res.data);
                },
                errorFn = function(res) {
                    deferred.reject(res);
                },
                httpObj = {
                    method: 'POST',
                    url: '/api/notification/read'
                };

            $http(httpObj)
                .then(successFn, errorFn);

            return deferred.promise;
        };

        fn.prototype.clear = function() {
            this.unread = [];
        };

        return fn;
    };


    angular
        .module('volupio')
        .factory('notifySvc', ['$http', '$q', '$rootScope', '$timeout', svcFn])
})();

'use strict';

angular.module('volupio').
        factory('notificationSvc',['$q', '$rootScope',
    function($q, $rootScope) {
        /*var list = [];
        return {
            list: list,
            add: function(dto){
                list.push(dto);
            }
        };*/

        var svc = function(){
            this.results = [];
        };

        svc.prototype.add = function(message, avatar, state) {
            var self = this,
                fn = function(){
                    self.svc.results.push({
                        message: message,
                        avatar: avatar,
                        state: state
                    });
                };
                
            return fn();
        };

        svc.prototype.remove = function(index) {
            var self = this,
                fn = function(){

                };

            return fn();
        };
    }]);

