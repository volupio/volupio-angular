/**
 * @ngdoc overview
 * @name volupio.feedback
 * 
 * @description
 * feedback module handle comments, rating, likes
 */
var feedback = angular.module('volupio.feedback', ['volupio']);
feedback.service('feedbackSvc', ['Restangular', '$q', function(Restangular, $q) {
	var feedbackSvc = {};

	/**
	 * @param {mongo_id} docId Document id
	 * @param {string} docType Document type, ie: auction, news, news,etter
	 * @param {string} message Comment message
	 */
	feedbackSvc.sendComment = function(docId, docType, message) {
		var defer = $q.defer(),
			req = {
				id: docId,
				message: message
			};

		Restangular.all(docType).one('feedback').one(docId).one('comment').customPOST(req, '').then(function(res) {
			defer.resolve(res);
		}, function(res) {
			defer.reject(res);
		});

		return defer.promise;
	};

	feedbackSvc.getComments = function(docId, docType, skip, take) {
		Restangular.all(docType).one('feedback').all('comment').then(function(res) {

		}, function(res) {

		});
	};

	feedbackSvc.removeComment = function(docId, docType) {

	}

	return feedbackSvc;
}]);