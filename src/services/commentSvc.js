(function(){
	/**
	 * @ngdoc service
	 * 
	 * @name Comment Service
	 * @description 
	 *
	 * This service is a factory for others components
	 * Each component is identified with a unique namespace, required to build the factory
	 */
	var svcFn = function($http, $q) {

		var fn = function(namespace){
			this.namespace = namespace;

			this.create = function(id, message) {
				var deferred = $q.defer(),
					successFn = function(res) {
						deferred.resolve(res.data);
					},
					errorFn = function(res) {
						deferred.reject(res);
					},
					httpObj = {
						method: 'POST',
						url: '/api/comment/' + this.namespace,
						data: {
							id: id,
							message: message
						}
					};

					$http(httpObj)
						.then(successFn, errorFn);

					return deferred.promise;
			};

			this.get = function(id, skip, take) {
				var deferred = $q.defer(),
					successFn = function(res) {
						deferred.resolve(res.data);
					},
					errorFn = function(res) {
						deferred.reject(res);
					},
					httpObj = {
						method: 'GET',
						url: '/api/comment/' + this.namespace + '/' + id
					};

					if(!_.isUndefined(skip)) {
						httpObj.url = httpObj.url + '&skip=' + skip;
					}
					if(!_.isUndefined(take)) {
						httpObj.url = httpObj.url + '&take=' + take;
					};

					$http(httpObj)
						.then(successFn, errorFn);

					return deferred.promise;
			};
		};
		return fn;
	};

	angular
		.module('volupio')
		.factory('commentFactory', ['$http', '$q', svcFn]);
})();