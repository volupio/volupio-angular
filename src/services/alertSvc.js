(function(){

    var svcFn = function($rootScope, $timeout) {

        // Alerts are kept in this array, and bindided from ui
        var alerts = [];
        
        var add = function(message, type) {
            var alert = {
                message: message,
                type: _.isUndefined(type) ? 'success' : type
            };
            return alerts.push(alert);
        };

        var addWithTimeout = function(message, type, duration) {
            var alert = {
                message: message,
                type: _.isUndefined(type) ? 'success' : type
            };
            var index = alerts.push(alert) - 1;
            $timeout(function(){
                alerts.splice(index, 1);
            }, _.isUndefined(duration) ? 5000 : duration);
        };

        var close = function(index) {
            alerts.splice(index, 1);
        };
        
        return {
            results: function(){
                return alerts;
            },
            close: function(index) {
                return close(index);
            },
            add: function(message, type) {
                return add(message, type);
            },
            addWithTimeout: function(message, type, duration) {
                return addWithTimeout(message, type, duration);
            }
        };
    };

    angular
        .module('volupio')
        .factory('alertSvc', ['$rootScope', '$timeout', svcFn]);
})();