(function(){
	var FileService = function($http, $q, modalSvc) {
		var svc = function(namespace){
			var self = this;
			self.namespace = namespace;

			self.get = function(){
				var deferred = $q.defer(),
					success = function(res) {
						deferred.resolve(res.data);
					},
					error = function(res) {
						deferred.reject(res);
					};

				$http.get('/api-beta/file/' + self.namespace)
					.then(success, error);

				return deferred.promise;
			};

			self.upload = function(){

			};
		};
	};
})