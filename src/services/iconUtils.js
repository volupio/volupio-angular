angular.module('volupio').
service('iconUtils', [function(){
	this.getClass = function(mimeType){
		switch(mimeType) {
			case 'image/jpeg':
				return 'fa-picture';
			break;
			case 'image/png':
				return 'fa-picture';
			break;
			case 'application/pdf':
				return 'fa-file';
			break;
			case 'application/msword':
				return 'fa-file';
			break;
		}
	};
}]).
directive('fileIcon', ['iconUtils', function(iconUtils){
	var linkFn = function(scope, element, attrs) {
		var a = iconUtils.getClass(scope.mimeType);
		element.addClass(a);
	};
	return {
		scope: {
			mimeType: "@"
		},
		replace: false,
		link: linkFn
	}
}]);