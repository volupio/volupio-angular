(function(){
    var serviceFn = function(){

        var svc = function(namespace){
            this.namespace = namespace;
            this.dictionary = [];
            this.modelAdd = {
                key: '',
                value: ''
            };
        };

        svc.prototype.save = function() {
            var dto = angular.copy(this.modelAdd);
            this.dictionary.push(dto);
            this.modelAdd.key = '';
            this.modelAdd.value = '';
        };

        svc.prototype.remove = function(key) {
            var self = this;
            angular.each(self.dictionary, function(v, k) {
                if(self.dictionary[k] === key) {
                    self.dictionary.splice(key, 1);
                }
            });
        };

        svc.prototype.get = function() {
            return this.dictionary;
        };

        return svc;
    };

    angular
        .module('volupio')
        .factory('metaFactory', serviceFn);
})();