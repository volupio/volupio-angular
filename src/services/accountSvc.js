/**
 * @ngdoc overview
 * @name volupio.account
 *
 * @requires volupio
 *
 * @description
 * volupio sub-module
 *
 * This module is a dependency of other module. Do not include this module as a dependency
 * in your angular app (use {@link volupio.account} module instead)
 */
var account = angular.module('volupio.account', ['volupio']);
account.service('accountSvc', ['Restangular', '$q', '$http', function(Restangular, $q, $http) {
        var accountSvc = {};

        accountSvc.confirm = function(id, token) {
            var deferred = $q.defer();
            $http.get('/account/confirm?id=' + id + '&token=' + token).
                then(function(res) {
                    deferred.resolve(res.data);
                }, function(res) {
                    deferred.reject(res);
                });
            return deferred.promise;
        }

        /**
         * @ngdoc function
         * @name volupio.account.login
         * 
         * @description
         * Authenticates the user with email and password
         *
         * @param {string} email Email address
         * @param {string} password Password
         */
        accountSvc.login = function(email, password, recaptcha) {
            var defer = $q.defer(),
                    req = {
                        email: email,
                        password: password
                    };

            if(!_.isUndefined(recaptcha)) {
                req.recaptcha = recaptcha;
            }
            Restangular.one('login')
                    .customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });
            return defer.promise;
        };
        /**
         * @ngdoc function
         * @name volupio.account.logout
         *
         * @description
         * Log out authenticated user, removing the access token and related cookies
         * This implies a redirect to '/logout'
         *
         * @param {string} redirect URI to which user should be redirect after the logout
         */
        accountSvc.logout = function(redirect) {
            window.location = '/logout';
        };

        /**
         * @ngdoc function
         * @name volupio.account.basicRegistration
         *
         * @description
         * Register a new regular user account
         */
        accountSvc.basicRegistration = function(firstName, lastName, email, password, passwordConfirm, packageId) {
            var defer = $q.defer(),
                    req = {
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password,
                        passwordConfirm: passwordConfirm,
                        packageId: packageId || null
                    };

            Restangular.one('register')
                    .customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res.data);
            });

            return defer.promise;
        };

        accountSvc.register = function(model) {
            var defer = $q.defer(),
                    req = angular.copy(model);

            Restangular.one('register')
                    .customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res.data);
            });

            return defer.promise;
        };
        /**
         * @ngdoc function
         * @name volupio.account.getBasicInfo
         *
         * @description
         * Get the basic information for current user (ie: name, email, about me)
         */
        accountSvc.getBasicInfo = function() {
            var defer = $q.defer();

            Restangular.one('account').one('info').get().then(function(res) {
                defer.resolve(res.originalResponse);
            }, function(res) {
                defer.resolve(res);
            });

            return defer.promise;
        };

        /**
         * @ngdoc function
         * @name volupio.account.saveBasicInfo
         *
         * @description
         * Update basic information 
         *
         * @param {string} firstName First name
         * @param {string} lastName Last name
         *
         * @return {object} responseStatus Api response 
         */
        accountSvc.saveBasicInfo = function(firstName, lastName) {
            var defer = $q.defer(),
                    req = {
                        firstName: firstName,
                        lastName: lastName
                    };

            Restangular.one('account').one('info')
                    .customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };
        /**
         * @ngdoc function
         * @name volupio.account.changePassword
         * 
         * @description
         * Update account password
         *
         * @param {string} currentPassword Actual password
         * @param {string} newPassword New password
         * @param {string} newPasswordConfirm Confirmation of the new password
         */
        accountSvc.changePassword = function(currentPassword, newPassword, newPasswordConfirm) {
            var defer = $q.defer(),
                    req = {
                        currentPassword: currentPassword,
                        newPassword: newPassword,
                        newPasswordConfirm: newPasswordConfirm
                    };

            Restangular.one('account').one('password')
                    .customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });
            return defer.promise;
        };
        /**
         * @ngdoc function
         * @name volupio.account.recoverPassword
         *
         * @description
         * Recover account password. The confirmation link is sent by email
         *
         * @param {string} email Primary account email
         */
        accountSvc.recoverPassword = function(email) {
            var defer = $q.defer(),
                    req = {
                        email: email
                    };
            Restangular.
                    one('account').
                    one('recover').
                    customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };
        /**
         * /api/account/recover/send
         * @returns {undefined}
         */
        accountSvc.sendRecoverPassword = function(token, email, password, passwordConfirm) {
            var defer = $q.defer(),
                    req = {
                        password: password,
                        passwordConfirm: passwordConfirm,
                        token: token,
                        email: email
                    };

            return Restangular.
                    one('account').
                    one('recover').
                    customPOST(req, 'send');

        };

        /**
         * Requests a new password
         * 
         * Send a recover account email
         * 
         * @param {string} email
         */
        accountSvc.getRecoverPassword = function(email) {
            var req = {
                email: email
            };
            return Restangular.
                    one('account').
                    one('recover').
                    customPOST(req, '');
        };
        
        accountSvc.setProfile = function(id, token, profile) {
            var deferred = $q.defer(),
                    req = {
                        id: id,
                token: token,
                profile: profile
                    };
            Restangular.one('account/profile').
                    customPOST(req, '').then(function(res) {
                       deferred.resolve(res.originalResponse); 
                    }, function(res) {
                        deferred.reject(res);
                    });
            return deferred.promise;
        };

        accountSvc.cancelAccount = function(reason) {
            var deferred = $q.defer(),
                    req = {
                        reason: reason
                    };
            Restangular.one('account/cancel').
                    customPOST(req, '').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        accountSvc.removeAvatar = function() {
            var deferred = $q.defer();

            Restangular.one('account/avatar').
                    customDELETE('', {}).then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });
            return deferred.promise;
        };



        /**
         * Get account delivery information
         */
        accountSvc.getDeliveryInfo = function(){
            var deferred = $q.defer();

            Restangular.one('account/delivery').get().then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });
 
            return deferred.promise;
        };

        /**
         * Update account delivery information
         * @return promise
         */
        accountSvc.saveDeliveryInfo = function(req) {
            var deferred = $q.defer();
            Restangular.one('account').customPOST(req, 'delivery').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };



        /**
         * Get account billing information
         */
        accountSvc.getBillingInfo = function(){
            var deferred = $q.defer();

            Restangular.one('account/billing').get().then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });
 
            return deferred.promise;
        };

        /**
         * Update account billing information
         * @return promise
         */
        accountSvc.saveBillingInfo = function(req) {
            var deferred = $q.defer();
            Restangular.one('account').customPOST(req, 'billing').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        /**
         * Get account company information
         */
        accountSvc.getCompanyInfo = function(){
            var deferred = $q.defer();

            Restangular.one('account/company').get().then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });
 
            return deferred.promise;
        };

        /**
         * Update account company information
         * @return promise
         */
        accountSvc.saveCompanyInfo = function(req) {
            var deferred = $q.defer();
            Restangular.one('account').customPOST(req, 'company').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        accountSvc.getAddressInfo = function(){
            var deferred = $q.defer();

            Restangular.one('account/address').get().then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });
 
            return deferred.promise;
        };

         accountSvc.saveAddressInfo = function(req) {
            var deferred = $q.defer();
            Restangular.one('account').customPOST(req, 'address').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        accountSvc.getContactInfo = function(){
            var deferred = $q.defer();

            Restangular.one('account/contact').get().then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });
 
            return deferred.promise;
        };

         accountSvc.saveContactInfo = function(req) {
            var deferred = $q.defer();
            Restangular.one('account').customPOST(req, 'contact').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        accountSvc.changeEmail = function(emailCurrent, emailNew, emailConfirm, password) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '/api/account/email',
                data: {
                    emailCurrent: emailCurrent,
                    emailNew: emailNew,
                    emailConfirm: emailConfirm,
                    password: password
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });
            return deferred.promise;
        };
        return accountSvc;
    }]);