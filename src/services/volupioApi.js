/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('volupio').service('volupioApi', ['Restangular', '$q', function(Restangular, $q) {
        var svc = { };
        svc.removeUser = function(userId) {
          return Restangular.all('user').one(userId).remove();
        };
        
        /**
         * @description
         * 
         * Check the avaibility for a email registration
         * This function is used by directives like registerEmailStatus
         * 
         * @param {string} email
         * @returns {boolean|null} True if email is available and can be registered
         */
        svc.emailAvaibility = function(email) {
            var deferred = $q.
            Restangular.all('email').one('check').customGET({ email: email}, '').then(function(res) {
                // In fact it inst false because the email could be available
                // This error must be handled
                var available = _.isBoolean(res.isAvailable) ? res.isAvailable : false;
                deferred.resolve(res.isAvailable);
            }, function(res) {
                deferred.reject(res);
            });
        };
        return svc;
}]);