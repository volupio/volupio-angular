'use strict';
/**
 * @ngdoc overview
 * @name volupio.user
 * @required volupio 
 *
 * @description
 * Manage registered users
 */
var user = angular.module('volupio.user', ['volupio']);
user.service('userSvc', ['Restangular', 'volupioApi', '$q', '$http', function(Restangular, volupioApi, $q, $http) {
        var userSvc = {};

        /**
         * @ngdoc function
         * @name volupio.user.getBasicInfo
         *
         * @description
         * Get the basic information for current user (ie: name, email, about me)
         */
        userSvc.getBasicInfo = function(id) {
            var deferred = $q.defer();

            $http({
                url: '/api/account/info',
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        /**
         * @ngdoc method
         * @name volupio.user.userSvc.find
         *
         * @description 
         * find users 
         * 
         * Get a list of registered users
         *
         * @param {int} skip Skip
         * @param {int} take Take
         */
        userSvc.find = function(skip, take) {
            var defer = $q.defer();
            var req = {
                skip: skip,
                take: take
            };
            Restangular.all('users').customGET('', req).then(function(res) {
                defer.resolve(res.originalResponse);
            }, function(res) {
                defer.reject(res);
            });
            return defer.promise;
        };

        userSvc.search = function(queryModel) {
            var deferred = $q.defer();

            Restangular.all('users').
                    customGET('', queryModel).
                    then(function(res) {
                        deferred.resolve(res.originalResponse);
                    }, function(res) {
                        deferred.reject(res);
                    });

            return deferred.promise;
        };
        /**
         * @ngdoc function
         * @name volupio.user.userSvc.get
         *
         * @description
         * Get a user by id
         *
         * @param {objectid} id user id
         */
        userSvc.get = function(id) {
            var defer = $q.defer();

            Restangular.
                    all('user').
                    one(id).
                    one('info').
                    get().
                    then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    });

            return defer.promise;
        };

        userSvc.confirmAdmin = function(id) {
            var deferred = $q.defer();

            Restangular.all('user').one(id).
                    customPOST({}, 'confirmadmin').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        userSvc.getStates = function() {
            var states = [
                {id: 1, name: 'Confirmação Email Pendente'},
                {id: 2, name: 'Confirmado'},
                {id: 3, name: 'Suspenso'},
                {id: 4, name: 'Banido'},
                {id: 5, name: 'Aguarda Alteração Password'},
                {id: 6, name: 'Cancelado'},
                {id: 7, name: 'Atribuição de Perfil'},
                {id: 8, name: 'Aguarda Administração'}
            ];
            return states;
        };
        
        /**
         * @ngdoc function
         * @name volupio.user.userSvc.create
         *
         * @description
         * Creates a new account
         *
         * @param {string} firstName First name
         * @param {string} lastName Last name
         * @param {string} email Email address
         * @param {boolean} requirePasswordChange When the user login for the first time, he'll be required to change his password
         */
        userSvc.create = function(firstName, lastName, email, permission) {
            var defer = $q.defer();
            var req = {
                firstName: firstName,
                lastName: lastName,
                email: email,
                permission: permission
            };
            Restangular.all('user').customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });
            return defer.promise;
        };

        userSvc.post = function(model, profile) {
            var req = angular.copy(model),
                deferred = $q.defer();

            req.profile = profile;

            var successFn = function(res) {
                deferred.resolve(res.data);
            },
            errorFn = function(res) {
                deferred.reject(res);
            },
            httpObj = {
                url: '/api/user',
                method: 'POST',
                data: req
            };

            $http(httpObj)
                .then(successFn, errorFn);

            return deferred.promise;
        };

        /**
         * @ngdoc method
         * @name save
         * @description
         * Updates a user basic information, including first name and last name
         * Other information like contacts and permissions have their own update endpoints
         *
         * @param {objectid} userId Id of user to be updated
         * @param {string} firstName User first name
         * @param {string} lastName User last name
         */
        userSvc.save = function(userId, firstName, lastName) {
            var defer = $q.defer(),
                    req = {
                        firstName: firstName,
                        lastName: lastName
                    };
            Restangular.all('users').one(userId).customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });
            return defer.promise;
        };

        userSvc.saveCredentials = function(userId, currentPassword, newPassword, newPasswordConfirm) {
            var deferred = $q.defer(),
                    req = {
                        currentPassword: currentPassword,
                        newPassword: newPassword,
                        newPasswordConfirm: newPasswordConfirm
                    };
            Restangular.all('user')
                    .one(userId)
                    .customPOST(req, 'credentials')
                    .then(function(res) {
                        deferred.resolve(res);
                    }, function(res) {
                        deferred.reject(res);
                    });
        };

        /**
         * @ngdoc overview
         * @name get user information controller
         * 
         * @description
         * Get the controller model for editing user information
         * 
         * @param {object_id} userId
         * @returns {$q@call;defer.promise}
         */
        userSvc.getInfo = function(userId) {
            var deferred = $q.defer();

            Restangular.all('user')
                    .one(userId)
                    .one('info')
                    .get()
                    .then(function(res) {
                        var model = {
                            firstName: res.firstName,
                            lastName: res.lastName,
                            birthday: res.birthday
                        };
                        deferred.resolve(model);
                    }, function(res) {
                        deferred.reject(res);
                    });

            return deferred.promise;
        };

        /**
         * Update user information
         * 
         * @param {object_id} userId
         * @param {object} model
         * @returns {$q@call;defer.promise}
         */
        userSvc.putInfo = function(userId, model) {
            var deferred = $q.defer();

            Restangular.all('user')
                    .one(userId)
                    .customPOST(model, 'info')
                    .then(function(res) {
                        deferred.resolve(res);
                    }, function(res) {
                        deferred.reject(res);
                    });

            return deferred.promise;
        };

        /**
         *  Remove a user by his id
         *  Once remove, the user can't be restored and all references are removed
         *  
         * @param {object_id} userId id of user to be removed
         * @returns {$q@call;defer.promise}Remove a user
         */
        userSvc.remove = function(userId) {
            var deferred = $q.defer();

            volupioApi.removeUser(userId).then(function(res) {
                deferred.resolve(res);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        userSvc.changePassword = function(userId, newPassword, newPasswordConfirm) {
            var deferred = $q.defer(),
                    req = {
                        newPassword: newPassword,
                        newPasswordConfirm: newPasswordConfirm
                    };
            Restangular.all('user')
                    .one(userId)
                    .customPOST(req, 'password')
                    .then(function(res) {
                        deferred.resolve(res);
                    }, function(res) {
                        deferred.reject(res);
                    });
            return deferred.promise;
        };

        userSvc.getProfile = function(userId) {
            var deferred = $q.defer();
            Restangular.
                    all('user').
                    one('user').
                    one(userId).
                    one('profile').
                    get().
                    then(function(res) {
                        deferred.resolve(res);
                    }, function(res) {
                        deferred.reject(res);
                    });
            return deferred.promise;
        };

        userSvc.cancelAccount = function(userId, reason) {
            var deferred = $q.defer(),
                    req = {
                        reason: reason
                    };

            Restangular.all('user').one(userId).one('cancel')
                    .customPOST(req, '').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        userSvc.getContact = function(id) {
            var deferred = $q.defer();
            $http({
                url: '/api/account/contact',
                method: 'GET',
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        userSvc.saveContact = function(id, model) {
            var deferred = $q.defer(),
                    req = angular.copy(model);
            
            $http({
                url: '/api/account/contact',
                method: 'POST',
                params: {
                    id: id
                },
                data: model
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });
            
            return deferred.promise;
        };


        /**
         * Get account delivery information
         */
        userSvc.getDeliveryInfo = function(id) {
            var deferred = $q.defer();

            $http({
                url: '/api/account/delivery',
                method: 'GET',
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        /**
         * Update account delivery information
         * @return promise
         */
        userSvc.saveDeliveryInfo = function(id, req) {
            var deferred = $q.defer();
            $http({
                url: '/api/account/delivery',
                method: 'POST',
                params: {
                    id: id
                },
                data: req
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });
            
            return deferred.promise;
        };



        /**
         * Get account billing information
         */
        userSvc.getBillingInfo = function(id) {
            var deferred = $q.defer();

            $http({
                url: '/api/account/billing',
                method: 'GET',
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        /**
         * Update account billing information
         * @return promise
         */
        userSvc.saveBillingInfo = function(id, req) {
            var deferred = $q.defer();
            
            $http({
                url: '/api/account/billing',
                method: 'POST',
                params: {
                    id: id
                },
                data: req
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });
            

            return deferred.promise;
        };

        /**
         * Get account company information
         */
        userSvc.getCompanyInfo = function(id) {
            var deferred = $q.defer();

            $http({
                url: '/api/account/company',
                method: 'GET',
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });


            return deferred.promise;
        };

        /**
         * Update account company information
         * @return promise
         */
        userSvc.saveCompanyInfo = function(id, req) {
            var deferred = $q.defer();
            
            $http({
                url: '/api/account/company',
                method: 'POST',
                params: {
                    id: id
                },
                data: req
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });
            

            return deferred.promise;
        };

        userSvc.getAddressInfo = function(id) {
            var deferred = $q.defer();

            $http({
                url: '/api/account/address',
                method: 'GET',
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        userSvc.saveAddressInfo = function(id, req) {
            var deferred = $q.defer();
            
            $http({
                url: '/api/account/address',
                method: 'POST',
                params: {
                    id: id
                },
                data: req
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });
            
            
            return deferred.promise;
        };

        userSvc.getContactInfo = function(id) {
            var deferred = $q.defer();
            $http({
                url: '/api/account/contact',
                method: 'GET',
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        userSvc.saveContactInfo = function(id, req) {
            var deferred = $q.defer();
            
            $http({
                url: '/api/account/contact',
                method: 'POST',
                params: {
                    id: id
                },
                data: req
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });
            
            return deferred.promise;
        };

        userSvc.getName = function(id) {
            var url = '/api/account/name';
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: url,
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });
            return deferred.promise;
        };

        userSvc.saveName = function(id, firstName, lastName, displayName) {
            var deferred = $q.defer();
             $http({
                method: 'POST',
                url: '/api/account/name',
                data: {
                    firstName: firstName,
                    lastName: lastName,
                    displayName: displayName
                },
                params: {
                    id: id
                }
            }).success(function(res) {
                deferred.resolve(res);
            }).error(function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        /**
         * @name Create a new user account
         * 
         * @description
         * Register a new user account without the user interaction
         * An email is sent with the generated password
         */
        userSvc.create = function(model) {
            var deferred = $q.defer(),
                successFn = function(res) {
                    deferred.resolve(res.data);
                },
                errorFn = function(res) {
                    deferred.reject(res);
                };


            $http
                .post('/api/user/', model)
                .then(successFn, errorFn);

            return deferred.promise;
        };

        /**
         * @name Update user state
         *
         * @description
         * There're already functions to cancel the account, confirm, etc
         * This method should be used as a last instance, it just change the state without dealing with important stuff like sending emails
         */
        userSvc.changeState = function(userId, state) {
            var deferred = $q.defer(),
                successFn = function(res) {
                    deferred.resolve(res.data);
                },
                errorFn = function(res) {
                    deferred.reject(res);
                };
            $http
                .post('/api/user/' + userId + '/state', {state: state})
                .then(successFn, errorFn);

            return deferred.promise;
        };

        return userSvc;
    }]);