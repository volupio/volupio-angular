/**
 * http://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
 * Same used as here: http://detectmobilebrowsers.com/
 */
window.mobilecheck = function() {
  var check = false;
  (function(a,b){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}

Date.prototype.fromISO8601 = function (isoDateString) {
    var parts = isoDateString.match(/\d+/g);
    var isoTime = Date.UTC(parts[0], parts[1] - 1, parts[2], parts[3], parts[4], parts[5]);
    var isoDate = new Date(isoTime);

    return isoDate;
};

/**
 * Underscore extension
 */
_.mixin({
    capitalize: function(string) {
        return string.charAt(0).toUpperCase() + string.substring(1);
    }
});

/**
 * A clone of the Node.js util.inherits() function. This will require
 * browser support for the ES5 Object.create() method.
 *
 * @param {Function} ctor
 *   The child constructor.
 * @param {Function} superCtor
 *   The parent constructor.
 */
angular.inherits = function (ctor, superCtor) {
  ctor.super_ = superCtor;
  ctor.prototype = Object.create(superCtor.prototype, {
    constructor: {
      value: ctor,
      enumerable: false
    }
  });
};


/**
 * Volupio Module
 * Main module used in all our angular apps
 *
 * @version 0.0.1 - 2014-04-01
 * @author Volupio <webmaster@volupio.com>
 */

angular.
        module('volupio', [
            'volupio.inbox',
            'restangular',
            'ui.router',
            'ngResource']).
        /**
         * @name volupio config
         * @description Configuration model
         *
         */
        constant('volupioConfig', {
            /* Base api url for providers like Restangular */
            baseApiUrl: '/api',
            /* Base pages url for templates */
            basePagesUrl: '/pages',
            baseDomain: 'volupio.com',
            /* By default, all our apps are designed for html 5 */
            appHtml5Mode: true,
            /* Profile required state */
            profileRequiresState: 'profileRequired'
        }).
        config(['$httpProvider', 'RestangularProvider', 'volupioConfig', '$locationProvider', '$urlRouterProvider',
            function($httpProvider, RestangularProvider, volupioConfig, $locationProvider, $urlRouterProvider) {

                $urlRouterProvider.otherwise('/');

            /*    var httpStatusCodeInterceptor = function($q) {
                    function onSuccess(response) {
                        return response.data;
                    }
                    ;

                    function onError(response) {
                        //$log.info('javascript error');
                    }
                    ;

                    return function(promise) {
                        return promise.then(onSuccess, onError);
                    }
                };
                $httpProvider.responseInterceptors.push(httpStatusCodeInterceptor);
*/
                /* Identify all http requests as ajax, setting the X_REQUESTED_WITH header interpreted by most APIs */
                $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
                /* For POST requests, our API look for form data and nothing else */
                $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
                /* Set the base url for Restangular */
                RestangularProvider.setBaseUrl(volupioConfig.baseApiUrl);

                /* All Restangular models have their own cache providers */
                var httpRestangularInterceptorFactory = function(data, operation, what, url, response, deferred) {
                    var newResponse = data;
                    newResponse.originalResponse = angular.copy(data);
                    return newResponse;
                };
                RestangularProvider.addResponseInterceptor(httpRestangularInterceptorFactory);

                /* Html5 Mode */
                $locationProvider.html5Mode({
                    enabled: volupioConfig.appHtml5Mode,
                    requireBase: false
                });

            }]).
        run(['$rootScope', 'modalSvc', '$location', function($rootScope, modalSvc, $location) {

            if (!_.isEmpty(($location.search()).error) ) {
                var msg = !_.isEmpty(($location.search()).message) ? ($location.search()).message : 'Ocorreu um erro, pedimos desculpa';
                $location.search({}); // Clear the URL query string, only available with html5 mode
                modalSvc.display({message: msg});
            }
            }]);