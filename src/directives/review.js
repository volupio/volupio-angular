angular.module('volupio').
	service('reviewEntitySvc', ['$q', function($q) {
		var svc = function(){
			this.total = 0;
			this.average = 0;
		};

		svc.prototype.set = function(total, average) {
			var fn = function(){
				svc.total = total;
				svc.average = average;
			};

			return fn;
		};

		return svc;
	}]).
	directive('reviewEntity', ['$rootScope', 'reviewEntitySvc', function($rootScope, reviewEntitySvc) {
		var linkFn = function(scope, element, attrs) {
			var svc = new reviewEntitySvc();
		}

		return {
			link: flinkFn,
			scope: {
				'review-total': '=',
				'review-average': '=',
				'review-entity': '='
			}
		};
	}]);