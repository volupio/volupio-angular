(function(){

	var directiveFn = function(volupioRegisterSvc, $q){

        var linkFn = function(scope, element, attrs) {
            scope.svc = volupioRegisterSvc;
            scope.m = {};
            scope.m.birthday = {dropdownSelector: '#birthdayDropdown',minView: 'day',startView: 'year'};
        };

        var cntrlFn = function($scope) {
            $scope.register = function(){
                var successFn = function(res) {
                    var fn = scope.registerSuccess();
                    fn(res);
                }, errorFn = function(res) {
                    var fn = scope.registerFailure();
                    fn(res);

                };

                volupioRegisterSvc
                    .register()
                    .then(successFn, errorFn);
            };    
        };

/**
            scope: {
                registerSuccess: '&registerSuccess',
                registerFailure: '&registerFailure',
                register: '&register'
            },
            */
        cntrlFn.$inject = ['$scope'];
        return {
            link: linkFn,
            controller: cntrlFn,
            controllerAs: 'registerCntrl',
            scope: {
                registerSuccess: '&registerSuccess',
                registerFailure: '&registerFailure',
                'registerCntrl.register': '&register'
            }
        }
    };

    var directiveSubmitFn = function(volupioRegisterSvc, $q) {
        var linkFn = function(scope, element, attrs, parentCtrl){
            var clickFn = function(){
                parentCtrl.register();
            };
            element.bind('click', clickFn);
        };

        linkFn.$inject = ['scope', 'element', 'attrs', 'parentCtrl'];
        return {
            replace: false,
            link: linkFn
        };
    };

    var volupioFn = function(accountSvc, $q, modalSvc) {
        var svc ={};

        svc.registerModel = {
            birthday: null
        };

        svc.register = function(){
            var request = angular.copy(svc.registerModel),
                deferred = $q.defer();

            accountSvc.register(request)
                .then(function(res) {
                    deferred.resolve(res);
                }, function(res) {
                    svc.registerModel.password = '';
                    svc.registerModel.passwordConfirm = '';
                    //svc.registerAgree = false;
                    deferred.reject(res);
                });
            return deferred.promise;
        };

        return svc;
    };

    angular
    	.module('volupio')
    	.directive('volupioRegisterSubmit', ['volupioRegisterSvc', '$q', directiveSubmitFn])
        .directive('volupioRegister', ['volupioRegisterSvc', '$q', directiveFn])
        .factory('volupioRegisterSvc', ['accountSvc', '$q', 'modalSvc', volupioFn]);
})();