(function(){
    var directiveFn = function(){
        return {
            link: function(scope, element, attrs, ctrl) {
                element.find('.upload-trigger').click(function() {
                    element.find('input[type="file"]').click();
                });
            }
        };
    };

    angular
        .module('volupio')
        .directive('uploadTrigger', directiveFn)
})();