(function(){

    var directiveFn = function(){

        var linkFn = function(scope, elem, attrs) {
            elem.attr('src', '/uploads/user/' + attrs.userSrc + '_thumb.jpeg');
        };

        return {
            replace: false,
            link: linkFn
        };

    };

    angular
        .module('volupio')
        .directive('userSrc', directiveFn)
})();