(function(){
	var directiveFn = function(){
	    return function(scope, element, attrs){
	        var url = attrs.backImg;
	        element.css({
	            'background-image': 'url(' + url +')',
	            'background-size' : 'cover'
	        });
	    };
	};
	angular
		.module('volupio')
		.directive('backImg', directiveFn);
})();