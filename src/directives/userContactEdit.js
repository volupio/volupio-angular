(function(){
	var directiveFn = function(userSvc, $q, modalSvc){
		var linkFn = function(scope, elem, attrs){
			elem.bind('click', function(){
				var id = scope.userId,
					deferred = $q.defer(),
					 successFn = function(res){
					 	//scope.confirmed(id);
					 	var fn = scope.userEditSuccess();
					 	if(!_.isUndefined(fn)) {
							fn(res);
						}
					 	else {
					 		modalSvc.display(res);
					 	}
					 	deferred.resolve(attrs);
					},
					errorFn = function(res){
						var fn = scope.userEditError();
						if(!_.isUndefined(fn)) {
							fn(res);
						}
						else {
							modalSvc.display(res);
						}
						deferred.reject(res);
					};

	            userSvc.saveAddressInfo(id, model)
	                .then(successFn, errorFn);

				return deferred.promise;
			});
		};

		return {
			link: linkFn,
			replace: false,
			scope: {
				userId: '@',
				userContactEditSuccess: '&',
				userContactEditError: '&'
			}
		};
	};

	angular
		.module('volupio')
		.directive('userContactEdit', ['userSvc', '$q', 'modalSvc', directiveFn]);
})();