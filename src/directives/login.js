/**
 * Volupio Login
 *
 * Provider: basic digest (username/email, password)
 */

(function(){

  var AUTH_EVENTS = {
      loginSuccess: 'auth-login-success',
        loginFailed: 'auth-login-failed',
        logoutSuccess: 'auth-logout-success',
        sessionTimeout: 'auth-session-timeout',
        notAuthenticated: 'auth-not-authenticated',
        notAuthorized: 'auth-not-authorized',
        /*
         * Account was registered but the app requires to be set a profile
         */
        profilePending: 'profile-pending',
        /*
         * Account was registered and profile also if required. Admin has to confirm yet
         */
        adminPending: 'admin-pending'
  };

  var ACCOUNT_STATE = {
      confirmed: 'confirmed',
      pendingAdmin: 'pending-admin'
  };

  var loginSvc = function(accountSvc, $rootScope, AUTH_EVENTS, $q) {

    return {
      basic: function(email, password, recaptcha) {
        var deferred = $q.defer(),
            successFn = function(res) {
                  $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, res.data);
                  deferred.resolve(res);
              },
            errorFn = function(res) {
                if (res.data.apiErrorCode === 'ProfileRequired') {
                    $rootScope.$broadcast(AUTH_EVENTS.profilePending, res.data);
                } else {
                  $rootScope.$broadcast(AUTH_EVENTS.loginFailed, res.data);
                }
                deferred.reject(res.data);
            };

           accountSvc.login(email, password, recaptcha)
             .then(successFn,errorFn);

           return deferred.promise;
        }
    };

  };

  var volupioLogin = function(loginSvc, modalSvc) {

    var linkFn = function(scope, element, attrs) {
        scope.account = {};
        /* @todo load default from cookie */

        scope.login = function() {
          var successFn = function(res) {
              var fn = scope.loginSuccess();
              fn(res);
          },
          errorFn = function(res) {
              scope.account.password = '';
              var fn = scope.loginFailure();
              fn(res);
          }
          if(!_.isUndefined(scope.account.recaptcha)) {
              loginSvc
            .basic(scope.account.email, scope.account.password, scope.account.recaptcha.challenge)
            .then(successFn, errorFn);
          } else {
            loginSvc
              .basic(scope.account.email, scope.account.password)
              .then(successFn, errorFn);
          }

        }

    };
    return {
        templateUrl: "/volupio/login.html",
  //                    transclude: true,
        link: linkFn,
        scope: {
          loginSuccess: '&loginSuccess',
          loginFailure: '&loginFailure'
        }
    };
  };

  var volupioLoginSubmit = function(){
    var linkFn = function(scope, element, attrs) {
        element.bind('click', function(){
          scope.login(); // volupio-login
        });
    };
    return {
        replace: false,
        link: linkFn
    }
  };
  angular.module('volupio').
    constant('AUTH_EVENTS', AUTH_EVENTS).
    constant('ACCOUNT_STATE', ACCOUNT_STATE).
    factory('loginSvc', ['accountSvc', '$rootScope', 'AUTH_EVENTS', '$q', loginSvc]).
        directive('volupioLogin', ['loginSvc', 'modalSvc', volupioLogin])
      .directive('volupioLoginSubmit', volupioLoginSubmit);
})();
