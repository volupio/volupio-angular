/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('volupio').
        factory('resetPasswordSvc', ['accountSvc', '$q', '$modal', 'modalSvc',
            function(accountSvc, $q, $modal, modalSvc) {
                return {
                    open: function() {
                        $modal.open({
                            templateUrl: 'resetPassword.html',
                            controller: 'resetPasswordCtrl'
                        }).result.then(function(res) {
                            modalSvc.display(res);
                        }, function(res) {
                            modalSvc.display(res);
                        });
                    },
                    send: function(email) {
                        var deferred = $q.defer();
                        accountSvc.recoverPassword(email).then(function(res) {
                            modalSvc.display(res);
                            deferred.resolve(res);
                        }, function(res) {
                            modalSvc.display(res);
                            deferred.reject(res);
                        });
                        return deferred.promise;
                    }
                };
            }]).
        controller('resetPasswordCtrl', ['$modalInstance', 'resetPasswordSvc', '$scope',
            function($modalInstance, resetPasswordSvc, $scope) {
                $scope.form = {};
                $scope.resetPassword = {};
                $scope.send = function() {
                    if ($scope.form.resetPassword.$valid) {
                        resetPasswordSvc.send($scope.resetPassword.email).then(function(res) {
                            $modalInstance.close(res);
                        });
                    }
                };
                
                $scope.cancel = function(){
                    $modalInstance.dismiss();
                };
            }]);
