angular.module('volupio').
        directive('formSummary', [function(){
                var linkFn = function(scope, element, attributes) {
                    scope.errors = scope.response.validationErrors;
                    scope.body = _.isEmpty(scope.response.description) ? scope.response.message : scope.response.description;
                    scope.show = !_.isUndefined(scope.response) && scope.response.show;
                };
            return {
                templateUrl: '/html/form/summary.html',
                link: linkFn,
                scope: {
                    response: '='
                }
            };
        }]);