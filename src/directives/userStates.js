(function(){
	var directiveFn = function($rootScope) {

		var _template = '<select>' +
							'<option value="">Escolha um estado</option>' +
		                    '<option ng-repeat="s in states" value="{{s.key}}">{{s.value}}</option>' +
		                    '</select>';
		var	linkFn = function(scope, element, attrs) {
	    	scope.states = [
	    		{ key: 1, value: 'Confirmação Pendente'},
	    		{ key: 2, value: 'Confirmado'},
	    		{ key: 3, value: 'Suspenso'},
	    		{ key: 4, value: 'Banido'},
	    		{ key: 5, value: 'Aguarda Alteração de Password'},
	    		{ key: 6, value: 'Cancelado'},
	    		{ key: 7, value: 'Aguarda Atribuição de Perfil'},
	    		{ key: 8, value: 'Aguarda Confirmação da Administração'}
	    	];
	    };
	 	return {
	 		template: _template,
	        link: linkFn
		};
	};

	var stateSvc =  function($q) {
        var svc = function(){
            this.states = [{ id: 1, text: 'Created' }, { id: 2, text: 'Pending' }, { id: 3, text: 'Confirmed' }];
        };

        svc.prototype.display = function(stateId){
            switch(stateId) {
                case 1:
                    return 'Email por confirmar';
                case 2:
                    return 'Confirmado';
                case 3:
                    return 'Suspenso';
                case 4:
                    return 'Banido';
                case 5:
                    return 'Aguardar aleração da password';
                case 6:
                    return 'Cancelada';
                case 7:
                    return 'Aguardar perfil';
                case 8:
                    return 'Aguardar confirmação Admin'
            }
            return 'N/A';
        };

        return svc;
    };

	angular
		.module('volupio')
		.directive('userStates', ['$rootScope', directiveFn])
		.service('userStateSvc', ['$q', stateSvc]);
})();