angular.
        module('volupio').
        directive('dateFormat', function(dateFilter) {
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {

                    ngModel.$formatters.push(formatter);
                    ngModel.$parsers.push(parser);

                    element.on('change', function (e) {
                        var element = e.target;
                        element.value = formatter(ngModel.$modelValue);
                    });

                    function parser(value) {
                        var m = moment(value);
                        var valid = m.isValid();
                        ngModel.$setValidity('datetime', valid);
                        if (valid) return m.valueOf();
                        else return value;
                    }

                    function formatter(value) {
                        if(_.isUndefined(value)) {
                            return null;
                        }
                        var m = moment(value);
                        var valid = m.isValid();
                        if (valid) return m.format("L");
                        else return value;

                    }

                }
            }
        });