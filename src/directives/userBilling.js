(function(){
	/**
	 * User Billing
	 */


	 var editDirective = function(userBillingSvc) {
	 	var linkFn = function(scope, element, attrs) {
	 		var id = scope.userId();
	 		scope.svc = new userBillingSvc(id);
	 		scope.model = {};

	 		var getSuccessFn = function(res){
	 				
	 					scope.svc.setModel(res);
	 				
		 		},
		 		getErrorFn = function(res){
		 			alert('gui');
		 		};

	 		scope.svc.get()
	 			.then(getSuccessFn, getErrorFn);

	 		scope.save = function(){
	 			var successfn = function(res) {
	 				scope.svc.saveSuccess();
	 			},
	 			errorFn = function(res) {
	 				alert('error');
	 			};
	 			scope.svc.save(scope.model)
	 				.then(successfn, errorFn);

	 		};
	 	};
	 	return {
	 		link: linkFn,
	 		scope: {
	 			saveSuccess: '&',
	 			userId: '&'
	 		}
	 		//templateUrl: '/volupio/user/billingEdit.html'
	 	}
	 };

	 var editSubmitDirective = function(){
	 	var linkFn = function(scope, element, attrs) {
 			element.bind('click', function(){
 				scope.save();
 			});
	 	};
	 	return {
	 		replace: false,
	 		link: linkFn
	 	}
	 };

	 var svcFn = function(userSvc, $q){
	 	/**
	 	 * @param id user id
	 	 */

	 	var svc = function(id){
	 		var self = this;
	 		this.id = id;
	 		this.model = {};
	 		this.setModel = function(m) {
	 			self.model = m;
	 		}
	 	};

	 	svc.prototype.save = function(model){
			return userSvc.saveBillingInfo(this.id, model);
		};

		svc.prototype.get = function(){
			return userSvc.getBillingInfo(this.id);
 		};

	 	return svc;
	 };

	 angular
	 	.module('volupio')
	 	.directive('userBillingEdit', ['userBillingSvc', editDirective])
	 	.directive('userBillingSubmit', editSubmitDirective)
	 	.factory('userBillingSvc', ['userSvc', '$q', svcFn]);
})();