/* 
 * Volupio 2014
 */

/**
 * @ngdoc module
 * @name register
 * @description
 * 
 * # register
 * 
 * Module for new registrations
 */
angular.module('volupio.register', ['volupio']).
        /**
         * @ngdoc service
         * @name regiterSvc
         * 
         * @description
         * 
         */
        factory('registerSvc', ['accountSvc', 'modalSvc', function(accountSvc, modalSvc) {
                return {
                    /**
                     * @ngdoc method
                     * @name register
                     * 
                     * @description
                     * Create a new account
                     * 
                     * @param {string} firstName
                     * @param {string} lastName
                     * @param {string} email
                     * @param {string} password
                     * @param {string} passwordConfirm
                     * @param {string} packageId null for default package
                     * @returns {undefined}
                     */
                    register: function(firstName, lastName, email, password, passwordConfirm, packageId) {
                        accountSvc.basicRegistration(firstName, lastName, email, password, passwordConfirm, packageId)
                                .then(function(res) { // Success
                                    window.location = '/';
                                }, function(res) { // Rejected
                                    modalSvc.display(res);
                                });
                    }
                }
            }]).
        directive('register', ['accountSvc', function(registerSvc) {
                return {
                    require: 'ngModel',
                    link: function(scope, element, attrs, c) {
                        scope.register = function() {
                            registerSvc.register(scope.firstName, scope.lastName, scope.email, scope.password, scope.passwordConfirm);
                        };
                    }
                }
            }]).
        /**
         * @ngdoc directive
         * @name Register Email Status
         * @description
         * 
         * Validator for registration with email
         * Check the email registration status with the volupio api
         * Uses accountSvc to retrieve the status
         * 
         */
        directive('avaibility', ['accountSvc', function(accountSvc) {
                return {
                    require: 'ngModel',
                    link: function(scope, elem, attrs, c) {
                        scope.$watch(attrs.ngModel, function() {
                            accountSvc.emailAvaibility(attrs).then(function(res) { // Success response
                                c.$setValidity('unique', res)
                            }, function(res) { // Failure response
                                c.$setValidity('unique', false)
                            })
                        });
                    }
                };
            }]);