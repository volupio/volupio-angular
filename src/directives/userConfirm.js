(function(){
	var directiveFn = function(userSvc, $q, modalSvc){
		var linkFn = function(scope, elem, attrs){

		

			elem.bind('click', function(){
				var id = scope.userId,
					deferred = $q.defer(),
					 successFn = function(res){
					 	//scope.confirmed(id);
					 	var fn = scope.userConfirmSuccess();
					 	fn(res);
					 	deferred.resolve(attrs);
					},
					errorFn = function(res){
						var fn = scope.userConfirmError();
						if(!_.isUndefined(fn)) {
							fn(res);
						}
						else {
							modalSvc.display(res);
						}
						deferred.reject(res);
					};

				userSvc.confirmAdmin(id)
					.then(successFn, errorFn);

				return deferred.promise;
			});
		};

		return {
			link: linkFn,
			replace: false,
			scope: {
				userId: '@',
				userConfirmSuccess: '&',
				userConfirmError: '&'
			}
		};
	};

	angular
		.module('volupio')
		.directive('userConfirm', ['userSvc', '$q', 'modalSvc', directiveFn]);
})();