/**
 * @author Guilherme cardoso <emaill@guilhermecardoso.pt>
 *
 * Directives to create and listen comments.
 * Volupio based APIs use a unique namespace for each component, ie: user, article
 */
(function(){
	
	/**
	 * @ngdoce directive
	 *
	 * @name Comment submit
	 * @description
	 *
	 * Submit a new comment to the API
	 */
	var submitDirective = function(commentSvc, $rootScope, $q){
		
		var linkFn = function(scope, elem, attrs) {
			var svc = new commentSvc(scope.namespace);

			scope.submit = function(){
				var deferred = $q.defer(),
					successFn = function(res) {
						var fn = scope.submitSuccess();
						fn(res);
						deferred.resolve(res);
					},
					errorFn = function(res) {
						var fn = scope.submitError();
						fn(res);
						deferred.reject(res);
					};

				commentSvc
					.post(scope.id, scope.message)
					.then(successFn, errorFn);	

				return deferred.promise;
			};

		};

		return {
			link: linkFn,
			scope: {
				'submitSuccess': '&submitSuccess',
				'submitError': '&submitError',
				'namespace': '=',
				'id': '='
			},
			replace: false
		};
	};

	/**
	 * @ngdoce directive
	 *
	 * @name Comment list
	 * @description
	 *
	 * List comments
	 */
	var listDirective = function(commentSvc, $rootScope, $q) {
		var svc = new commentSvc(scope.namespace);
		var linkFn = function(scope, elem, attrs){
			var successFn = function(res) {

					// Call listNoMore function from parent scope
					if(res.comments.length == 0) {
						var fn = scope.listNoMore();
						fn(res);
						return;
					}

					angular.copy(res.comments, scope.comments);
				},
				errorFn = function(res) {
					var fn = scope.listError();
					fn(res);
				};

			svc
				.get(id, scope.comments.length, 25)
				.then(successFn, errorFn);
		};

		return {
			link: linkFn,
			scope: {
				'listError': '&listError',
				'listNoMore': '&listNoMore',
				'comments': '&comments',
				'namespace': '=',
				'id': '='
			},
			replace: false
		};
	};

	angular
		.module('volupio')
		.directive('commentSubmit', ['commentSvc', '$rootScope', '$q', submitDirective])
		.directive('commentList', ['commentSvc', '$rootScope', '$q', submitDirective]);
})();