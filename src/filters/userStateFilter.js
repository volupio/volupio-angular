(function(){

	var fn = function($filter){
		return function(input) {
			switch(input) {
				case 1:
					return 'Confirmação Pendente';
				case 2:
					return 'Confirmado';
				case 3:
					return 'Suspenso';
				case 4:
					return 'Banido';
				case 5:
					return 'Aguardar Alteração Password';
				case 6:
					return 'Cancelado';
				case 7:
					return 'Aguardar Atribuição Perfil';
				case 8:
					return 'Aguardar Confirmação Administração';
				case 99:
					return 'READONLY';

				default:
					return 'N/A';
			}
		};
	};

	fn.$inject = ['$filter'];

	/**
	 * @name User State Filter
	 *
	 * @description
	 * 
	 * Filter to display the user state from AcountState enumerator
	 */
	angular
		.module('volupio')
		.filter('userStateFilter', fn)
})();