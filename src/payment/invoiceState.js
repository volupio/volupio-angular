angular.module('volupio.payment').
	factory('invoiceStateSvc', function(){
		var svc = function(){

		};

		svc.prototype.stateDisplay = function(invoiceId){
			switch(invoiceId) {
			 	case 1:
			 		return 'Created';
			 	case 2:
			 		return 'Waiting';
			 	case 3:
			 		return 'Confirmed';
			 	case 4:
			 		return 'Canceled';
			 	case 5:
			 		return 'Expired';
                case 6:
                    return 'On Going';
			}
			return 'Default';
		};
	})