angular.module('volupio.payment').
	service('invoiceApi', ['$http', '$q', function($http, $q) {

		this.get = function(id){
			return $http.get('/api/invoice/' + id);
		};

		this.find = function(queryModel) {
			return $http({
				url: '/api/invoice', 
				method: 'GET',
				params: queryModel
			});
		};
		this.post = function(model) {
			return $http.post('/api/invoice', model);
		};

		this.postConfirm = function(id) {
			return $http.post('/api/invoice/' + id + '/confirm');
		};

        this.postOnGoing = function(id) {
            return $http.post('/api/invoice/' + id + '/ongoing');
        }
	}]);