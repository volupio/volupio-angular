(function(){
    var configFn = function(){

    };

    var runFn = function($rootScope, auctionFollowSvc){
    	$rootScope.followSvc = auctionFollowSvc;
    };

    var fifiConfig = {

        wsEnabled: true,
        wsHost: '',
        wsAppId: 'b8de27e42b2d4c60e868',
        reputationVariant: 0.3,
        positionVariant: 0.7
    };
    var AUCTION_STATE = {
        'created': 1,
        'published': 2,
        'activate': 3,
        'completed': 4,
        'trash': 5,
        'pendingAccept': 6,
        'pendingSummary': 7
    };

    var ACCOUNT_STATE = {
        'pendingConfirmation': 1,
        'confirmed': 2,
        'suspended': 3,
        'banned': 4,
        'pendingPasswordChange': 5,
        'canceled': 6,
        'pendingProfile': 7,
        'pendingAdminConfirmaion': 8,
        'readonly': 99
    };

    var AUCTION_FOLLOW_STATE = {
        /* The auction hasn't started yet, it still has time left */
        'waiting': 0,
        /* Auction started but not bidded yet, it's pristine*/
        'pristine': 1,
        /* Auction started and the last bid was other user */
        'loosing': 2,
        /* Auction started and the last bid was the current user */
        'winning': 3,
        /* Auction has finished and the user didn't won */
        'lost': 4,
        /* Auction has finished and the user won */
        'won': 5
    };


    var profileType = {
        bidder: 1,
        contractor: 2,
        admin: 99
    };

    angular
        .module('fifi', ['volupio', 'pusher-angular'])
        .config(configFn)
        .run(['$rootScope', 'auctionFollowSvc', runFn])
        .constant('fifiConfig', fifiConfig)
         .constant('PROFILE_TYPE', profileType)
        .constant('AUCTION_STATE', AUCTION_STATE)
        .constant('ACCOUNT_STATE', ACCOUNT_STATE)
    /**
     * @ngdoc constant
     * @name jobsApp.AUCTION_FOLLOW_STATE
     *
     * @description
     * This states are for following auctions or active auctions
     * The directives will use the state to call the user atention with danger, warning classes
     * I've decided to also track before and after auction, in order to keep the recents auctions on the list
     */
        .constant('AUCTION_FOLLOW_STATE', AUCTION_FOLLOW_STATE)
        .provider("biddyConfiguration", function () {
            var appId = '', 
                appSecret = '',
                appKey = '',
                port = 4567,
                wsHost = 'pusher.com';

            this.setAppId = function (id) {
                appId = id;
            };

            this.setAppSecret = function(secret) {
                appSecret = secret;
            };

            this.setAppKey = function(key) {
                appKey = key;
            };

            this.setPort = function(port) {
                port = port;
            };

            this.setWsHost = function(host) {
                wsHost = host;
            };

            this.$get = function () {
                return {
                    wsHost: wsHost,
                    appId: appId,
                    appSecret: appSecret,
                    appKey: appKey,
                    port: port
                };
            };
        });
})();