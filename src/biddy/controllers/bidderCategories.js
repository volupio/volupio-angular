(function(){
    var serviceFn = function($rootScope, $q, bidderSvc, modalSvc) {
                
        var getController = function() {
            var deferred = $q.defer();
            bidderSvc.getMyCategoriesCtrl().then(function(res) {
                deferred.resolve(res.data);
            }, function(res) {
                modalSvc.display(res);
            });
            return deferred.promise;
        };

        var add = function(id) {
            var deferred = $q.defer();
            bidderSvc.addCategory(id).then(function(res) {
                deferred.resolve(res.data);
                modalSvc.display(res.data);
            });
            return deferred.promise;
        };

        var remove = function(id){
            var deferred = $q.defer();
            bidderSvc.delCategory(id).then(function(res) {
                deferred.resolve(res.data);
                modalSvc.display(res.data);
            });
            return deferred.promise;
        };

        return {
            getController: getController,
            add: add,
            remove: remove
        };
    };

    serviceFn.$inject = ['$rootScope', '$q', 'bidderSvc', 'modalSvc'];

    var cntrlFn = function($scope, svc) {
        // Already registered categories
        $scope.userCategories = [];
        $scope.view = 'grid';
        $scope.userCategoriesIds = [];
        // All available categories for the user register
        $scope.categories = [];
        
        svc.getController().then(function(res) {
            $scope.usersCategories = res.userCategories;
            $scope.categories = res.categories;
            $scope.userCategoriesIds = res.userCategoriesIds;
            angular.forEach($scope.categories, function(value, key) {
               value['used'] = _.contains(res.userCategoriesIds, value.id);
            });
        });
        
        $scope.add = function(id) {
            svc.add(id).then(function(res) {
                $scope.userCategoriesIds.push(id);
                angular.forEach($scope.categories, function(value, key) {
                    if(value.id === id) {
                        $scope.categories[key]['used'] = true;
                    }
                });
            });
        };
        
        $scope.remove = function(id) {
            svc.remove(id).then(function(res) {
               for(var i = 0; i < $scope.userCategoriesIds.length; i++) {
                   if($scope.userCategoriesIds[i] === id) {
                       $scope.userCategoriesIds.splice(i, 1);
                   }
               }
               angular.forEach($scope.categories, function(value, key) {
                    if(value.id === id) {
                        $scope.categories[key]['used'] = false;
                    }
                });
            });
        };
    };

    cntrlFn.$inject = ['$scope', 'biddyBidderCategoriesSvc'];

    angular.module('fifi').
            factory('biddyBidderCategoriesSvc', serviceFn).
            controller('biddyBidderCategoriesCtrl', cntrlFn);
})();