(function(){

    var directiveFn = function(){

        var linkFn = function(scope, elem, attrs) {
            elem.attr('src', '/uploads/category/' + attrs.categoryId + '_thumb.jpeg');
        };

        return {
            replace: false,
            link: linkFn
        };

    };

    angular
        .module('fifi')
        .directive('categoryImgSrc', directiveFn)
})();