angular.module('fifi').
    directive('projectsFollowing', ['auctionFollowSvc', '$log', '$rootScope', 'modalSvc', function(auctionFollowSvc, $log, $rootScope, modalSvc) {
                return {
                    templateUrl: function(elem,attrs) {
                        return attrs.templateUrl || '/volupio/biddy/project/follow.html'
                    },
                    replace: false,
                    scope: {
                        total: '='
                    },
                    controller: ['$scope', function($scope) {

                        $scope.userId = $rootScope.userId;

                        $scope.$on('bid', function(event, res) {
                            var a = false, // aux project found
                            i = 0, // aux project index
                            id = res.projectId; // project id
                            angular.forEach($scope.projects, function(value, key) {
                                if(value.id === id && !auctionFollowSvc.isCurrentView(value.id)) {
                                    a = true;
                                    i = key;
                                }
                                if(a) {
                                    return false;
                                }
                            });

                            if(a) {
                                $scope.$apply(function(){
                                    $scope.projects[i].newBid = true;
                                });
                            }
                        });

                        $scope.endBidderProject = function(id){
                            modalSvc.confirm('Acabar', '<p>O projeto encontra-se em fase de revisão.</p><p>Pode terminar a sua participação, e será notificado quando for anunciado o vencedor do concurso.</p>', 'Terminar', 'Cancelar')
                                .then(function(res) {
                                   auctionFollowSvc.endBidderProject(id);
                                });
                        }
                    }]
                };
            }]);