(function(){
    angular.module('fifi').
            service('biddyProjectService', ['projectSvc', 'jobSvc', 'auctionFollowSvc', '$q', '$rootScope',
                function(projectSvc, jobSvc, auctionFollowSvc, $q, $rootScope) {
                
                this.isFollowingAuction = function(id){
                    return auctionFollowSvc.isFollowingAuction(id);
                };

            this.isFollowingProject = function(id) {
                return auctionFollowSvc.isFollowing(id);
            };
             

            }]).
            directive('biddyProject', ['projectSvc', 'jobSvc', '$sce', 'biddyProjectService', '$rootScope',
    function(projectSvc, jobSvc, $sce, biddyProjectService, $rootScope) {
        return {
            replace: false,
            link: function(scope, element, attrs) {
                var id = scope.$eval(attrs.project).id,
                        trusted = {};

                scope.isFollowingProject = function(){
                    return $rootScope.followSvc.isFollowing(id);
                };

                if(scope.isFollowing) {
                    console.log('following this project');
                }
                
                scope.getContentHtml = function(html) {
                    return trusted[html] || (trusted[html] = $sce.trustAsHtml(html));
                };

            }
        };
    }]);
})();