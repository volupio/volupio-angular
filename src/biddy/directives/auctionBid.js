(function(){

	 var bidDirective = function(modalSvc, jobSvc) {
        var cntrlFn = function($scope) {
            /**
             * Bid the auction
             * @param id {string} auction id
             * @param ammount {float} bid ammount
             */
            $scope.bid = function(id, ammount){
                $scope.canBid = false;
                $scope.busy = true;
                jobSvc.bidAuction(id, ammount).then(function(res) {
                    $scope.canBid = true;
                    $scope.busy = false;
                    $scope.userBid = null;
                    $scope.model.bidAmmount = '';
                }, function(res) {
                    modalSvc.display(res);
                    $scope.busy = false;
                    $scope.canBid = true;
                });
            };

            $scope.canBid = true;
            $scope.model = {};
        };

        return {
            replace: false,
            controller: ['$scope', cntrlFn]
        };
    };

	angular
	.module('fifi')
	.directive('auctionFollowBid', ['modalSvc', 'jobSvc', bidDirective])
})();