(function(){

    var directiveFn = function(){

        var linkFn = function(scope, elem, attrs) {
            elem.attr('src', '/uploads/user/' + attrs.bidderId + '_thumb.jpeg');
        };

        return {
            replace: false,
            link: linkFn
        };

    };

    angular
        .module('fifi')
        .directive('bidderImgSrc', directiveFn)
})();