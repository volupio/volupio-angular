angular.module('fifi').
        factory('adminSvc', ['$q', '$rootScope', 'usersPendingFactory', 'auctionFollowSvc',
    function($q, $rootScope, usersPendingFactory, auctionFollowSvc) {
        var svc = {};
        
        /**
         * Init method
         * 
         * Starts admin configuration
         * @param array model Init response returned by API
         * @returns {undefined}
         */
        svc.init = function(model) {
            var pendingSvc = new usersPendingFactory(parseInt(model.usersPendingAdmin));
            //$rootScope.usersPendingAdmin = model.usersPendingAdmin;
            auctionFollowSvc.setProjects(model.projectsActive);
            $rootScope.usersPendingSvc = pendingSvc;
            $rootScope.isAdmin = true;
        };
        
        return svc;
    }])
        .factory('usersPendingFactory', ['$rootScope', function($rootScope) {
                
                var svc = function(pendingCounter){
                    this.pendingCounter = pendingCounter;
                    this.increase = function(total) {
                        this.pendingCounter = this.pendingCounter + (_.isNumber(total) ? total : 1);
                    };
                    
                    this.decrease = function(total) {
                        this.pendingCounter = this.pendingCounter - (_.isNumber(total) ? total : 1);
                    };
                    
                };
        
                return svc;
        }]);
(function(){
    var svcFn = function($q, $http) {

        this.getController = function(){
            var deferred = $q.defer(),
                successFn = function(res) {
                    deferred.resolve(res.data);
                },
                errorFn = function(res) {
                    deferred.reject(res);
                },
                httpConfig = {
                    url: '/admin.json',
                    method: 'GET'
                };

            $http(httpConfig)
                .then(successFn, errorFn);

            return deferred.promise;
        }
    };

    angular
        .module('fifi')
        .service('adminFactory', ['$q', '$http', svcFn]);
})();