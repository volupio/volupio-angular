(function(){
    angular
    .module('fifi')
    .factory('projectListSvc', ['projectSvc', 'modalSvc', '$q', '$rootScope', '$modal', 'projectApi',
    function(projectSvc, modalSvc, $q, $rootScope, $modal, projectApi) {
        var menuCreateIndex;

        var svc = function(){
            var self = this;

            self.busy = false;
            self.results = [];
            self.modelCached = {};
            self.sortBy = 'name';
            self.sortOrder = 'asc';

            self.next = function(){
                self.modelCached.skip = self.results.length;
                return self.get(self.modelCached, false);
            };

            self.setSort = function(sortBy, sortOrder) {

                if(sortBy == self.sortBy) {
                    // reverse the order and keep the sort by
                    self.sortOrder = self.sortOrder == 'asc' ? 'desc' : 'asc';
                }
                else {
                    self.sortBy = sortBy ;
                    self.sortOrder = sortOrder;
                }

                return self.get(self.modelCached, true);
            };

            self.get = function(model, reset) {

                if(self.busy && !reset) return;
                self.busy = true;
                if(_.isUndefined(model)) 
                    model = {};

                model.sortBy = self.sortBy;
                model.sortOrder = self.sortOrder;

                var deferred = $q.defer(),
                    req = angular.copy(model) || {};

                if ($rootScope.isContractor && req.getMy == true) {
                    req.userId = $rootScope.userId;
                }
                if (_.isString(name)) {
                    req.name = name;
                }
                var skip = reset ? 0 : self.results.length;

                self.modelCached = req;
                projectSvc.find(skip, 10, req).
                    then(function(res) {

                        if(reset) {
                            self.results = [];
                        }


                        angular.forEach(res.results, function(value, key) {
                            self.results.push(value);
                        });

                        self.busy = false;
                        deferred.resolve(res);
                    }, function(res) {
                        deferred.reject(res);
                    });
                return deferred.promise;
            };
            self.getController = function() {
                var deferred = $q.defer();
                projectApi.getController().then(function(res) {
                    deferred.resolve(res.data);
                }, function(res) {
                    modalSvc.display(res);
                    deferred.reject(res);
                });
                return deferred.promise;
            };
            self.remove = function(id) {
                var deferred = $q.defer();
                projectSvc.remove(id).
                    then(function(res) {
                        modalSvc.display(res);
                        deferred.resolve(res);
                    }, function(res) {
                        modalSvc.display(res);
                        deferred.reject(res);
                    });
                return deferred.promise;
            };

            self.removeModal = function(id) {
                var instance = $modal.open({
                    templateUrl: '/web/project/remove.html',
                    controller: 'projectListRemoveCtrl',
                    resolve: {
                        projectId: function() {
                            return id;
                        }
                    }
                });
                return instance.result.promise;
            };
        };

        return svc;
    }]);
})();