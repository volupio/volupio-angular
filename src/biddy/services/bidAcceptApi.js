(function(){
	var service = function($http, $q) {

		this.acceptProject = function(bidId) {
			var deferred = $q.defer(),
				successFn = function(res) {
					deferred.resolve(res);
				},
				errorFn = function(res) {
					deferred.reject(res);
				},
				httpObj = {
					url: '/api/accept/project/' + bidId,
					method: 'POST'
				};

				$http(httpObj)
					.then(successFn, errorFn);

				return deferred.promise;
		};

		this.acceptAuction = function(bidId, aunctionId) {
			var deferred = $q.defer(),
				successFn = function(res) {
					deferred.resolve(res);
				},
				errorFn = function(res) {
					deferred.reject(res);
				},
				httpObj = {
					url: '/api/accept/auction/' + bidId + '/' + aunctionId,
					method: 'POST'
				};

				$http(httpObj)
					.then(successFn, errorFn);

				return deferred.promise;
		};

	};

	angular
		.module('fifi')
		.service('bidAcceptApi', ['$http', '$q', service]);
})();