angular.module('fifi').
    /*
     * While the auctionFollow allows one user to follow several auctions
     * This service handles the contractor data, containing the projects and auctions he own
     * Data from this service may be repetead in followSvc
     */
        service('contractorSvc', ['$rootScope', 'auctionFollowSvc', '$q', '$http',
            function($rootScope, auctionFollowSvc, $q, $http) {

                var data = {
                    projects: [],
                    auctions: []
                };

                var init = function(model) {
                    $rootScope.userRank = {
                        profile: 2,
                        auctionsPublished: 0,
                        totalEarnings: 0,
                        rankPosition: 3,
                        rankLevel: 0,
                        rankPercentage: 90
                    };
                    $rootScope.isContractor = true;
                    auctionFollowSvc.setProjects(model.projectsActive);
                    this

                };

                var ownProject = function(id) {

                };

                var ownAuction = function(id) {

                };

                var incrementCashFn = function(userId, amount){
                    var deferred  =$q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        },
                        request = {
                            amount: amount
                        };

                    $http({
                            method: 'POST',
                            url: '/api/contractor/cash',
                            params: {id: userId},
                            data: request
                        })
                        .then(successFn, errorFn);

                    return deferred.promise;
                };

                var getCashFn = function(userId) {
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        };
                   $http({
                       method: 'GET',
                       url: '/api/contractor/cash',
                       params: {
                           id: userId
                       }
                   })
                       .then(successFn, errorFn);
                    return deferred.promise;
                };

                return {
                    init: init,
                    getCash: getCashFn,
                    incrementCash: incrementCashFn,
                    ownProject: ownProject,
                    ownAuction: ownAuction,
                    getProjectsId: function(){
                        return data.projects;
                    },
                    getAuctionsId: function(){
                        return data.auctions;
                    }
                };

            }]).
factory('contractorData', ['$rootScope', '$q', function($rootScope, $q) {

    }]);
/*(function(){
    var svcFn = function($http, userSvc, $q) {
        this.postCash = function(id, amount){
            var deferred = $q.defer(),
                successFn = function(res) {
                    return deferred.resolve(res.data);
                },
                errorFn = function(res) {
                    return deferred.reject(res);
                };

            $http
                .post('/api/contrator/' + id + '/cash', {amount: amount})
                .then(successFn, errorFn);

            return deferred.promise;
        };

        this.getCash = function(id){
            var deferred = $q.defer(),
                successFn = function(res) {
                    return deferred.resolve(res.data);
                },
                errorFn = function(res) {
                    return deferred.reject(res);
                };

            $http
                .get('/api/contractor/' + id + '/cash')
                .then(successFn, errorFn);

            return deferred.promise;
        };
    };
     angular
         .module('fifi')
         .service('contractorSvc', ['$http', 'userSvc', '$q'. svcFn]);
})();*/