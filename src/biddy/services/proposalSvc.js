/* 
 * This file is part of the Fifi package
 *  (c) Volupio <webmaster@volupio.com>
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code
 */

angular.module('fifi')
    .service('proposalSvc', ['$http', '$q', 'Restangular', 'modalSvc', function($http, $q, Restangular, modalSvc) {
        var proposalSvc = {};

        proposalSvc.getCreateController = function(auctionId) {
            var defer = $q.defer();
            $http.get('/auction/' + auctionId + '/proposal/create').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };

        proposalSvc.create = function(auctionId, ammount) {
            var defer = $q.defer(),
                    req = {
                        auctionId: auctionId,
                        ammount: ammount
                    };
            Restangular.
                    all('auction').
                    one(auctionId).
                    one('proposal').
                    one('create')
                    .customPOST(req, '').
                    then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    })

            return defer.promise;
        };

        proposalSvc.findByAuction = function(auctionId, skip, take) {
            var defer = $q.defer();

            Restangular.all('auction').one(auctionId).one('proposal').customGET('').then(function(res) {
                defer.resolve(res);
            }, function(res) {

            });

            return defer.promise;
        };

        return proposalSvc;
    }]);