/*
 * AngularJS module for Volupio API's
 * This implementation follow guidelines that will be shared across several projects
 * The API is based on Laravel 
 */
var module;
module =  angular.module('fifi');

/*
 * Configuration for Volupio API
 */
module.provider('volupioRestConfig', function(){
    var restConfig;
    restConfig = {
    /*
     * Url Prefix. Ie: /api/v3/jobs
     */
      urlPrefix: "",
      /*
       * Max retries the client will retry when a internal server error is detected
       * Bad requests aren't retried 
       */
      maxRetries: 3,
      /*
       * Function to handle unauthorized exceptions 
       */
      unauthorizedFn: null
  };
  return {
    setRestConfig: function(newConfig) {
        return angular.extend(restConfig, newConfig);
    },  
    $get: function(){
        return restConfig;
    }
  };
});
module.factory('volupioApiClient', 
['volupioRestConfig', '$http', '$q', '$timeout', '$location', '$log', function(volupioRestConfig, $http, $q, $timeout, $location, $log){
     var restClient, RestResponse;
     /*RestResponse
      * This 
      */
     RestResponse = (function(){
         /*
          * Constructor
          */
         function RestResponse(response) {
             var _ref, _r1, _r2, _r3, _r4, _r5;
             this.response = response;
             /*
              * Success on the request depends on response status property. Between 200 and 300 is OK
              */
             this.success = (200 <= (_ref = this.response.status) && _ref < 300);
             this.statusCode = this.response.status;
             
             if(this.success) {
                 this.data = this.response.data;
             }
             else {
                 this.error = (_r1 = this.response) != null
                 ? (_r2 = _r1.data) != null
                    ? _r2.responseStatus
                    : void 0
                 : void 0;
             }
             this.headers = this.response.headers;
             this.config = this.response.config;   
         }
         RestResponse.prototype.getConfig = function(){
             return this.response.config;
         }
         RestResponse.prototype.hasValidationError = function(){
           var _ref;
           return ((_ref = this.validationErrors) != null 
           ? _ref.length
           : void 0) > 0;
         };
         /*
          * Indicates if the request was sucefully authenticated
          * @returns {Boolean}
          */
         RestResponse.prototype.isUnauthenticated = function(){
             return this.statusCode === 401;
         }
         
         return RestResponse;
     });
     
     
     /*
      * Rest client
      */
     restClient = (function(){
         function restClient() { }
         
         /*
          * This function clears a url
          * Code injection is validated at server side, but i'll make a few tweeks also on clientside
          * @param {type} url
          * @returns {String}
          */
         restClient.prototype.fixUrl = function(url) {
          var prefix, result;
          if (0 === url.indexOf(volupioRestConfig.urlPrefix)) {
            return url;
          } else {
            $log.log("Fixing url: " + url);
            prefix = serviceStackRestConfig.urlPrefix.replace(/\/+$/, "");
            url = url.replace(/^\/+/, "");
            result = "" + prefix + "/" + url;
            $log.log("to url: " + prefix + "/" + url);
            return result;
          }
        };
         /*
          * Delete method
          * Keep 'delete' as dictionary instead of object property because delete is reserved on IE
          */
         restClient.prototype['delete'] = function(url, config) {
             if(config == null)
                 config = {};
             config.method = 'DELETE';
             config.url = url;
             return this.execute(config);
         };
         /*
          * Get Method
          */
         restClient.prototype.get = function(url, config) {
             if(config == null)
                 config = {};
             config.method = 'GET';
             config.url = url;
             return this.execute(config);
         };
         /*
          * Post Method
          */
         restClient.prototype.post = function(url, data, config) {
             if(config == null)
                 config = {};
             config.method = 'POST';
             config.url = url
             config.data = data;
             return this.execute(config);
         };
         /*
          * Put method
          */
         restClient.prototype.put = function(url, config) {
             if(config == null)
                 config = {};
             config.method = 'PUT';
             config.url = url;
             return this.execute(config);
         };
         /*
          * Execute a API call
          */
         restClient.prototype.execute = function(config) {
             var successFn, validationFn, errorFn, defer, promise;
             sucessFn = [];
             validationFn = [];
             errorFn = [];
             successFn = [];
             defer = $q.defer();
             
             promise = $http(config);
             promise.then(function(response) {
                 
                 // Success
                 var result;
                 result = new RestResponse(response);
                 return defer.resolve(result);
             }, function(response) {
                 
             // Invalid
                 var resul = new RestResponse(response);
                 return defer.reject(result);
             });
             defer.promise.error = function(fn) {
                 errorFn.push(fn);
             }
             return defer.promise;
         };
         return restClient;
     });
     return new restClient();
}])
.service('apiSvc', ['$http', function($http) {
    return {
        get: function(){
            
        }
    };
}]);