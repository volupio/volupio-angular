angular.module('fifi').
        service('fifiAccountSvc', ['$q', 'Restangular', '$http',
            function($q, Restangular, $http) {

                /*
                 * Helper append the forms dtos to a unique request dto
                 * Each property is pre fixed by his form dto name.
                 * Ie: form.billing.name turns to billingName
                 */
                var appendDto = function(request, dto, name) {
                    angular.forEach(dto, function(value, key) {
                        request[name + _(key).capitalize()] = value;
                    });
                };

                return {
                    getCvFiles: function(id) {
                        var req = {},
                            deferred = $q.defer();

                        if(_.isUndefined(id)) {
                            req.id = id;
                        }

                        $http({method: 'GET', url: '/api/bidder/cv', params: req}).
                                success(function(data) {
                                    deferred.resolve(data);
                                }, function(data) {
                                    deferred.reject(data);
                                });
                        return deferred.promise;
                    },
                    removeCvFile: function(id, userId) {
                        var deferred = $q.defer();
                          var url = '/api/bidder/cv';
                            if(!_.isUndefined(userId)) {
                                url = url + '/' + userId;
                            }

                        $http({method: 'DELETE', url: url, data: {fileId: id}}).
                                success(function(data) {
                                    deferred.resolve(data);
                                }).error(function(res) {
                            deferred.reject(res);
                        });

                        return deferred.promise;
                    },
                    getCvFilesWithToken: function(userId, token) {
                        var deferred = $q.defer(),
                            successFn = function(res) {
                                deferred.resolve(res.data);
                            },
                            errorFn = function(res) {
                                deferred.reject(res);
                            },
                            httpConfig = {
                                method: 'GET',
                                url: '/api/bidder/cv/' + userId + '/' + token
                            };

                        $http(httpConfig)
                            .then(successFn, errorFn);
                        return deferred.promise;
                    },
                    removeCvFileWithToken: function(userId, fileId, token) {
                        var deferred = $q.defer();

                        $http({method: 'DELETE', url: '/api/bidder/cv/' + userId + '/' + token, data: {fileId: fileId}}).
                                success(function(data) {
                                    deferred.resolve(data);
                                }).
                                error(function(data) {
                                    deferred.reject(data);
                                });

                        return deferred.promise;
                    },
                    registerBidder: function(id, token, categories, contactDto, billingDto, addressDto, cvDescription, useCompany) {
                        var req = {
                        };
                        appendDto(req, contactDto, 'contact');
                        appendDto(req, billingDto, 'billing');
                        appendDto(req, addressDto, 'address');



                        req['id'] = id;
                        req['token'] = token;
                        req['categories'] = categories || [];
                        req['cvDescription'] = cvDescription;
                        req['useCompany'] = useCompany;
                        var deferred = $q.defer();
                        Restangular.one('bidder').
                                customPOST(req, 'confirm').
                                then(function(res) {
                                    deferred.resolve(res.originalResponse)
                                }, function(res) {
                                    deferred.reject(res);
                                });
                        /*
                         var _data = angular.copy(req);
                         _data.file = file;
                         
                         
                         $http({
                         method: 'POST',
                         url: '/api/bidder/confirm',
                         headers: {'Content-Type': false},
                         transformRequest: function(data) {
                         var formData = new FormData();
                         angular.forEach(req, function(key, value) {
                         if (key !== 'file') {
                         formData.append(key, angular.toJson(value));
                         }
                         });
                         formData.append('file', file);
                         return formData;
                         },
                         data: _data
                         }).
                         success(function(data, status, headers, config) {
                         deferred.resolve(data);
                         }).
                         error(function(data, status, headers, config) {
                         deferred.reject(data);
                         });
                         */

                        return deferred.promise;
                    },
                    registerContractor: function(id, token, contactDto, billingDto,  addressDto, useCompany) {
                        var req = {
                            useCompany: useCompany
                        };
                        appendDto(req, contactDto, 'contact');
                        appendDto(req, billingDto, 'billing');
                        appendDto(req, addressDto, 'address');


                        req['id'] = id;
                        req['token'] = token;
                        var deferred = $q.defer();
                        Restangular.one('contractor').
                                customPOST(req, 'confirm').
                                then(function(res) {
                                    deferred.resolve(res.originalResponse)
                                }, function(res) {
                                    deferred.reject(res);
                                });

                        return deferred.promise;
                    }
                };
            }]);