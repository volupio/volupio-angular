(function(){
	
	var resource = function($resource) {
		return $resource('/api/project/:id/meta');
	};

	resource.$inject =  ['$resource'];

	angular
		.module('fifi')
		.factory('ProjectMetaResource', resource);
})();