(function(){
	var svc = function($rootScope, bidderSvc, contractorSvc, adminSvc, ACCOUNT_STATE, AUCTION_STATE){
		this.init = function(model){
			$rootScope.ACCOUNT_STATE = ACCOUNT_STATE;
	        $rootScope.AUCTION_STATE = AUCTION_STATE;

            $rootScope.isAuthenticated = true;

			$rootScope.showLoginForm = false;
            $rootScope.userId = model.userId;
            var userDash = {
                displayName: model.userDisplayName,
                id: model.userId,
                uri: model.userUri,
                avatar: model.userAvatar,
                profileType: model.profileType,
                profileDisplay: model.profileDisplay
            }, userRank;
            $rootScope.userDash = userDash;
            $rootScope.isAdmin = model.profileType === 99;
            $rootScope.isBidder = model.profileType === 1;
            $rootScope.isContractor = model.profileType === 2;

            var now = new Date();
            var start = new Date(now.getTime() + 1*60000);
            var end = new Date(now.getTime() + 2*60000);
     
            switch (model.profileType) {
                case 1:
                    bidderSvc.init(model);
                    break;
                case 2:
                    contractorSvc.init(model);
                    break;
                case 99:
                    adminSvc.init(model);
                    break;
            }

            var modelRes = {
                userId: model.userId,
                userRank: userRank,
                userDash: userDash,
                profile: model.profileType,
                model: model
            };

            return modelRes;
		};
	};

	angular
		.module('fifi')
		.service('bootService', ['$rootScope', 'bidderSvc', 'contractorSvc', 'adminSvc', 'ACCOUNT_STATE', 'AUCTION_STATE', svc])
})();