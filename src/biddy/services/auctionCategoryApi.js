angular.module('fifi').
        service('auctionCategoryApi', ['$http', '$q', '$log', function($http, $q, $log) {
                this.post = function(dto) {
                    return $http({
                        url: '/api/category',
                        method: 'POST',
                        data: dto
                    });
                };
                
                this.get = function(){
                    return $http({
                        url: '/api/category',
                        method: 'GET'
                    });
                };
                
                this.getById = function(id) {
                    return $http({
                       url: '/api/category/' + id,
                       method: 'GET'
                    });
                };
                
                this.viewCtrl = function(id) {
                    return $http.get('/categories.json/' + id);
                };
            }]);