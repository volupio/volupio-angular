(function(){
	
	var svc = function($resource) {
		return $resource('/api/project/:projectId/followers', {});
	};

	svc.$inject = ['$resource'];

	angular
		.module('fifi')
		.factory('projectFollowersSvc', svc);

})();