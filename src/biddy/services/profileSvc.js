(function(){
    var svcFn = function($http, $q, $rootScope) {

        var svc = function(){

        };

        svc.prototype.getController = function(id) {
            if(_.isUndefined(id)) {
                id = $rootScope.userId;
            }

            var deferred = $q.defer(),
                successFn = function(res) {
                    deferred.resolve(res.data);
                },
                errorFn = function(res) {
                    deferred.reject(res);
                },
                httpConfig = {
                    url: '/profile.json',
                    params: { id: id }
                };

            $http(httpConfig)
                .then(successFn, errorFn);

            return deferred.promise;
        };

        return svc;
    };



    angular
        .module('fifi')
        .service('profileSvc', ['$http', '$q', '$rootScope', svcFn]);
})();