/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('fifi').
        factory('jobCategorySvc', ['$q', 'Restangular', 'modalSvc', function($q, Restangular, modalSvc) {
        var jobCategorySvc = {};

        jobCategorySvc.find = function(skip, take, childs, model) {

            var defer = $q.defer();
            var req = _.isUndefined(model) ? {} : model;
            req.skip = skip;
            req.take = take;
                    
            if(!_.isUndefined(childs)) {
                req.childs = childs;
            }
            Restangular.all('category').customGET('', req).then(function(res) {
                defer.resolve(res.originalResponse);
            }, function(res) {
                defer.reject(res);
            });
            return defer.promise;
        };

        jobCategorySvc.get = function(categoryId) {
            var defer = $q.defer();
            Restangular.all('category').one(categoryId).customGET('', {}).then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };

        jobCategorySvc.create = function(name, content) {
            var defer = $q.defer(),
                    request = {
                        name: name,
                        content: content
                    };

            Restangular.all('category').customPOST(request, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };

        jobCategorySvc.createWithParent = function(parentId, name, content) {
            var defer = $q.defer(),
                    request = {
                        name: name,
                        content: content,
                        parentId: parentId
                    };

            Restangular.all('auction').all('category').customPOST(request, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };

        jobCategorySvc.uploadThumbnail = function(categoryId) {
            var deferred = $q.defer();
            
            
            
            return deferred.promise;
        };
        
        jobCategorySvc.getInfo = function(categoryId){
            var deferred = $q.defer();
            
            Restangular.all('category').one(categoryId).customGET('info', '').then(function(res) {
                var model = {
                    name: res.name,
                    content: res.content
                };
                deferred.resolve(model);
            }, function(res) {
                deferred.reject(res);
            });
            
            return deferred.promise;
        };
        
        jobCategorySvc.saveInfo = function(categoryId, name, content) {
          var deferred = $q.defer(),
              req = {
                  name: name,
                  content: content
              };
          Restangular.all('category').one(categoryId).customPOST(req, 'info').then(function(res) {
              deferred.resolve(res);
          }, function(res) {
              deferred.res(res);
          });
          
          return deferred.promise;
        };
        
        /**
         * Remove a category
         * 
         * API: /api/auction/category/:id'
         * 
         * @param {type} categoryId
         * @returns {undefined}
         */
        jobCategorySvc.remove = function(categoryId) {
           var deferred = $q.defer();
           
           Restangular.all('auction').all('category').one(categoryId).remove().then(function(res) {
               deferred.resolve(res);
           }, function(res) {
               deferred.reject(res);
           });
            
           return deferred.promise;
        };

        return jobCategorySvc;
    }]);