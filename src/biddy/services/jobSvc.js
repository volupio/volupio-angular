function postJobReq() {
    this.name = '';
    this.content = '';
    this.minBudget = null;
    this.maxBudget = null;
    this.begin = null;
    this.end = null;
    this.categories = [];
}
/**
 * Job Service
 * 
 */
angular
    .module("fifi")
    .service('jobSvc', ['$http', '$q', 'Restangular', 'modalSvc', function($http, $q, Restangular, modalSvc) {

        
        var jobSvc = {};
        jobSvc.getAuction = function(id) {
            var defer = $q.defer();

            var job = Restangular.one('auction', id)
                    .get();
            defer.resolve(job);

            return defer.promise;
        };

        /**
         * return a job 
         * 
         * Return a job dy id
         * @param {string} id
         * @returns job dto
         */
        jobSvc.getById = function(id) {
            var deferred = $q.defer();
                var job = Restangular.one('jobs', id)
                        .get();
                deferred.resolve(job);
                /*$http.get('/api/jobs/' + id).success(function(res) {
                 deferred.resolve(res);
                 });*/
            return deferred.promise;
        };
        /**
         * return jobs
         * 
         * Get a list of jobs filtered
         * @returns array
         */
        jobSvc.getAll = function(queries) {
            var defer = $q.defer();
                Restangular.all('jobs').customGET('', queries)
                        .then(function(res) {
                            // Save results in cache
                            /*for(var i = 0; i < res.length; i++) {
                             jobCache.put(res[i].id, res[i]);
                             }*/
                            defer.resolve(res);
                        });
            return defer.promise;
        };
        jobSvc.getAuctionsByOwner = function() {
            var defer = $q.defer();
            Restangular.all('me/jobs').getList()
                    .then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    });
            return defer.promise;
        };
        jobSvc.getGroupsByOwner = function() {
            var defer = $q.defer();
            Restangular.all('me/groups').getList()
                    .then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    })
            return defer.promise;
        };


        /**
         * @ngdoc method
         * @name Create a auction for a group
         * 
         * @description
         * 
         * API: /api/group/:id/auction
         * 
         * @param {type} groupId
         * @param {type} model
         * @returns {$q@call;defer.promise}
         */
        jobSvc.createAuctionToGroup = function(groupId, model) {
            var deferred = $q.defer(),
                    req = angular.copy(model);

            Restangular.all('group').one(groupId).customPOST(req, 'auction').then(function(res) {
                deferred.resolve(res);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        }

        jobSvc.createAuctionForGroup = function(groupId, name, content, begin, end, minBudget, maxBudget, payment) {
            var defer = $q.defer();

            var req = {
                groupId: groupId,
                name: name,
                content: content,
                begin: begin,
                end: end,
                minBudget: minBudget,
                maxBudget: maxBudget,
                payment: payment
            };

            Restangular.all('jobs').post(req).then(function(res) {
                // jobCache.put(res.id, res);
                defer.resolve(res);
            });

            return defer.promise;
        }

        /**
         * @ngdoc method
         * @name save info
         * 
         * @description
         * Save basic information
         * 
         * @param object dto
         * @returns {unresolved}
         */
        jobSvc.updateAuctionInfo = function(id, name, excerpt, content) {
            var defer = $q.defer(),
                    req = {
                        id: id,
                        name: name,
                        excerpt: excerpt,
                        content: content
                    };

            Restangular.all('jobs').
                    one(req.id).
                    one('info').
                    customPOST(req, '').
                    then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    });

            return defer.promise;
        };

        /**
         * Update Auction @DECRAPTED
         
         jobSvc.saveAuction = function(id, name, content) {
         var defer = $q.defer();
         var putAuctionReq = {
         name: name,
         content: content
         };
         
         Restangular.oneUrl('jobs/' + id + '/info')
         //.withHttpConfig({ transformRequest: angular.identity })
         .customPOST(putAuctionReq, '').then(function(res) {
         modalSvc.display(res);
         defer.resolve(res);
         }, function(res) {
         modalSvc.display(res);
         defer.reject(res);
         });
         return defer.promise;
         };
         */
        /**
         * Controllers
         */
        jobSvc.auctionEditCtrl = function(id) {
            var defer = $q.defer();

            Restangular.
                    one('auction.json').
                    one(id).
                    one('edit').
                    get().
                    then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    });

            return defer.promise;
        };
        jobSvc.auctionCreateCtrl = function() {
            var defer = $q.defer();
            Restangular.
                    one('auction.json/create').
                    get().
                    then(function(res) {
                        defer.resolve(res);
                    }, function(res) {
                        defer.reject(res);
                    });
            return defer.promise;
        };

        jobSvc.auctionListCtrl = function() {
            var deferred = $q.defer();

            Restangular.
                    one('auction.json/list').
                    get().
                    then(function(res) {
                        deferred.resolve(res.originalResponse);
                    }, function(res) {
                        deferred.reject(res);
                    });

            return deferred.promise;
        };

        /**
         * @ngdoc function
         * @name auction view controller
         * 
         * @description
         * Get the request model 
         * 
         * @param {type} auctionId
         * @returns {unresolved}
         */
        jobSvc.auctionViewCtrl = function(auctionId) {
            var defer = $q.defer();
            Restangular.one('auction.json/view', auctionId).get().then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });
            return defer.promise;
        }

        /**
         * @ngdoce function
         * @name category create
         *
         * @description
         *
         * Create a new category
         * @param string name
         * @param string description
         * @param object_id|null parentId
         */
        jobSvc.categoryCreate = function(name, description, parentId) {
            var defer = $q.defer(),
                    req = {
                        name: name,
                        description: description,
                        parentId: parentId
                    };

            Restangular.all('category').customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };
        /**
         * @ngdoc function
         * @name category save
         *
         * @description
         * Update category basic information
         */
        jobSvc.categorySave = function(categoryId, name, description) {
            var defer = $q.defer(),
                    req = {
                        name: name,
                        description: description
                    };

            Restangular.all('category').one(categoryId).customPOST(req, '').then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };
        /**
         * @ngdoc function
         * @name category get
         *
         * @description
         * Get a category by id
         */
        jobSvc.categoryGet = function(categoryId) {
            var defer = $q.defer();

            Restangular.all('category').one(categoryId).get().then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            })

            return defer.promise;
        }

        jobSvc.categoryQuery = function(skip, take) {
            var defer = $q.defer();

            Restangular.all('category').getList().then(function(res) {
                defer.resolve(res);
            }, function(res) {
                defer.reject(res);
            });

            return defer.promise;
        };

        jobSvc.removeAuction = function(auctionId) {
            var deferred = $q.defer();

            Restangular.all('auction').one(auctionId).remove().then(function(res) {
                deferred.resolve(res);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        jobSvc.bidAuction = function(auctionId, ammount) {
            var deferred = $q.defer();
            var req = {
                ammount: ammount
            };
            Restangular.all('auction').one(auctionId).
                    customPOST(req, 'bid').
                    then(function(res) {
                        deferred.resolve(res.originalResponse);
                    }, function(res) {
                        deferred.reject(res);
                    });
            return deferred.promise;
        };

        jobSvc.getFollowing = function(userId) {
            var deferred = $q.defer();
            Restangular.all('auction').
                    one('like').
                    get().then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });
            return deferred.promise;
        };

        jobSvc.addFollowing = function(userId, auctionId) {
            var deferred = $q.defer(),
                    req = {
                        userId: userId
                    };
            Restangular.all('auction').one(auctionId).
                    customPOST(req, 'like').then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        jobSvc.removeFollowing = function(userId, auctionId) {
            var deferred = $q.defer(),
                    req = {
                        userId: userId
                    };
            Restangular.all('auction').one(auctionId).
                    customDELETE('like', req).then(function(res) {
                deferred.resolve(res.originalResponse);
            }, function(res) {
                deferred.reject(res);
            });
            return deferred.promise;
        };
        
        jobSvc.removeFile = function(auctionId, fileId) {
                        var deferred = $q.defer();
                        $http({
                            method: 'DELETE',
                            url: '/api/auction/' + auctionId + '/file/' + fileId
                        }).success(function(res) {
                            deferred.resolve(res);
                        }).error(function(res) {
                            deferred.reject(res);
                        });
                        
                        return deferred.promise;
                    };

        jobSvc.saveFile = function(auctionId, fileId, model) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '/api/auction/' + auctionId + '/file/' + fileId,
                data: model
            }).success(function(res) {
                deferred.resolve(res.data);
            }).error(function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };

        jobSvc.availableCtrl = function(auctionId) {
            return $http.get('/auction.json/' + auctionId +'/accept');
        };
        return jobSvc;
    }]);