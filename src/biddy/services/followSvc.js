(function(){
    /**
     * @ngdoc overview
     *
     * @description
     * The follow services stores the auctions and projects that the user is following
     * Those auctions don't have to be active
     * It handles the websocket connections with Pusher
     * Brodcast new bids through 'bid'
     *
     * @todo
     *  * abstract the websocket connections, once for all!
     */

    /**
     * @ngdoc factory
     * @name jobsApp.auctionFollowSvc
     */
    var svcFn = function($pusher, jobSvc, $q, modalSvc, $rootScope, $log, fifiConfig, AUCTION_FOLLOW_STATE, $timeout, $modal, projectSvc, biddyConfiguration) {
        /*
         * I keep the auctions and projects in this project
         * Use getProjects and getAuctions to return a reference of this
         * Changes are propagated through $.apply. The event is fired by the websockets, so it's needed
         */
        var svc = {};

        svc.wsEnabled = false;

        /*
         * Following auctions
         */
        svc.auctions = [];
        /*
         * Following projects
         */
        svc.projects = [];
        /*
         * Id of project that's in user screen
         * When the user is not viewing a project, it's null */
        svc.currentProjectView = null;

        var pusher = null,
            client = null;

        /**
         * @name bidProject
         * @eventType brodcast
         *
         * @description
         * Send a new bid
         * This function update the project and auction state, as well propagate the changes to others components with $brodcast
         * Others components will listen to 'bid' event
         */
        

        /**
         * Enable the Realtime updates through websockets
         * @param registerHandlers bool if true, will bind to pusher channels
         */
        var enableWs = function(registerHandlers){
            svc.wsEnabled = true;
            Pusher.log = function(message) {
                $log.debug(message);
            };

            client = new Pusher(biddyConfiguration.appKey);
            /*
            {
                wsHost: biddyConfiguration.wsHost,
                wsPort: biddyConfiguration.port,
                wssPort: "8080",
                enabledTransports: ['ws', 'flash']
            }
            */

            pusher = $pusher(client);
            $log.debug('created the pusher client');

            if(registerHandlers) { // check project stat
                angular.forEach(svc.projects, function(value, key) {
                    registerBidHandler(value.id);
                })
            }
            $log.debug('Websockets enabled');
        };  

        var disableWs = function(){

        };
        /**
         * @link ng.$rootScope.Scope#methods_$on listen
         * Callback for a new bid
         * Receive a event from websockets
         */
        $rootScope.$on('bid', function(event, res) {
            $log.debug('bid event received');
            var ab = false,
                projectIndex = null,
                auctionIndex = null;

            angular.forEach(svc.projects, function(item, key) {
                if (ab) {
                    return false;
                }
                angular.forEach(item.jobs, function(aitem, akey) {
                    if (aitem.id === res.auctionId) {
                        if(_.isUndefined(svc.projects[key].jobs[akey].bids)) {
                            svc.projects[key].jobs[akey].bids = [];
                        }
                        svc.projects[key].jobs[akey].bids.push({
                            userId: res.userId,
                            amount: res.ammount
                        });
                        svc.projects[key].jobs[akey].bidsCount = parseInt(svc.projects[key].jobs[akey].bidsCount) + 1;

                        svc.projects[key].jobs[akey].lastBidUserId = res.userId;
                        svc.projects[key].jobs[akey].lastBidAmount = res.ammount;
                        if (res.userId !== $rootScope.userId && !isCurrentView(svc.projects[key].jobs[akey].id)) {
                            svc.projects[key].jobs[akey].newBid = true;
                        }
                        else if (res.userId === $rootScope.userId) {
                            svc.projects[key].jobs[akey].userBid = res.ammount;
                        }
                        svc.projects[key].jobs[akey].bidState = res.userId === $rootScope.userId
                            ? AUCTION_FOLLOW_STATE.winning
                            : AUCTION_FOLLOW_STATE.loosing;

                        $log.info('$rootScope.$apply by auctionFollowSvc');

                        projectIndex = key;
                        angular.forEach(svc.auctions, function(ai, ak) {
                            if(ai.id === res.auctionId) {
                                auctionIndex = ak;
                                ab = true;
                                return false;
                            }
                        });
                    }
                });
            });

            if (false) { // Auction exists and the user isn't viewing the main project page
                $rootScope.$apply(function() {
                    svc.auctions[auctionIndex].lastBidUserId = res.userId;
                    svc.auctions[auctionIndex].lastBidAmount = res.ammount;
                    if (res.userId !== $rootScope.userId && !isCurrentView(svc.auctions[projectIndex].id)) {
                        svc.projects[projectIndex].newBid = true;
                        svc.auctions[auctionIndex].newBid = true;
                    }
                    else if (res.userId === $rootScope.userId) {
                        svc.auctions[auctionIndex].userBid = res.ammount;
                    }
                    svc.auctions[auctionIndex].bidState = res.userId === $rootScope.userId
                        ? AUCTION_FOLLOW_STATE.winning
                        : AUCTION_FOLLOW_STATE.loosing;

                    $log.info('$rootScope.$apply by auctionFollowSvc');
                    return false;
                });
            }
        });
        
        /**
         * @name Register Bid Handler
         *
         * @description
         * Binds the pusher handler for a new incoming bid
         */
        var registerBidHandler = function(projectId) {
            var channel = 'p' + projectId;
            var wsChannel = pusher.subscribe(channel);
            $log.debug('Subscribed ' + channel + ' channel');
            wsChannel.bind('bid', function(res) {
                $log.info('new bid incoming');
                angular.forEach(svc.projects, function(obj, indexProject) {
                    if (obj.id === res.projectId) {
                        angular.forEach(obj.jobs, function(objAuction, indexAuction) {
                            if (objAuction.id === res.auctionId) {
                                $log.info('starting brodcast');
                                /**
                                 * Broadcast to all listeners (directives, services, etc) */
                                $rootScope.$broadcast('bid', res);
                            }
                        });
                    }
                });
            });
            $log.debug('Registered bid handler for project ' + projectId);
        };


        var isCurrentView = function(id) {
            return !_.isNull(svc.currentProjectView) && svc.currentProjectView === id;
        };

        var removeAuctions = function(ids) {
            if (!_.isArray(ids)) {
                return false;
            }
            var p = 0; // track total pulled auctions to compare with array length
            angular.forEach(svc.auctions, function(value, key) {
                if (!_.isUndefined(value) && _.contains(ids, value.id)) {
                    p++;
                    this.splice(key, 1);
                    if (p === ids.length) {
                        return false;
                    }
                }
            }, svc.auctions);
        };

        var addAuction = function(dto) {
            dto.busy = false;
            svc.auctions.push(dto);
            $rootScope.userRank.auctionsDone = $rootScope.userRank.auctionsDone++;
        };

        var removeProject = function(id) {
            angular.forEach(svc.projects, function(value, key) {
                if (!_.isUndefined(value) && value.id === id) { // i don't know why but this foreach is looping more one than length, and value gets undefined
                    this.splice(key, 1);
                    return false;
                }
            }, svc.projects);
        };

        var endModal = function(id){

            angular.forEach(svc.projects, function(value, key) {

                if(value['id'] == id) {
                    value['state'] = 7;
                    value['stateDisplay'] = 'Revisão Pendente';

                    angular.forEach(value['jobs'], function(jValue, jKey) {
                        jValue['state'] = 7;
                        jValue['stateDisplay'] = 'Revisão Pendente';
                    });
                }
            });
            $modal.open({
                templateUrl: '/volupio-biddy/project/end.html',
                resolve: {
                    projectId: function(){
                        return id;
                    }
                },
                controller: ['$scope', 'projectId', '$modalInstance', 'projectSvc', function($scope, projectId, $modalInstance, projectSvc) {

                    projectSvc.get(projectId)
                        .then(function(dto) {
                            $scope.project = dto;
                        });

                    $scope.close = function(){
                        $modalInstance.close();
                    };
                }]
            });
        };

        var startModal = function(id) {
            
            var a = false; // aux to detect if any project is activated
            angular.forEach(svc.projects, function(value, key) {
                
                if(value['id'] == id) {
                    value['state'] = 3;
                    value['stateDisplay'] = 'Activo';
                    a = true;

                    angular.forEach(value['jobs'], function(jValue, jKey) {
                        jValue['state'] = 3;
                        jValue['stateDisplay'] = 'Activo';
                    });
                }
            });

            if(a && svc.wsEnabled != true) {
                enableWs();
            }

            $modal.open({
                templateUrl: '/volupio-biddy/project/start.html',
                resolve: {
                    projectId: function(){
                        return id;
                    }
                },
                controller: ['$scope', 'projectId', '$modalInstance', 'projectSvc', function($scope, projectId, $modalInstance, projectSvc) {

                    projectSvc.get(projectId)
                        .then(function(dto) {
                            $scope.project = dto;
                        });

                    $scope.close = function(){
                        $modalInstance.close();
                    };
                }]
            });
        };

        var registerEndHandler = function(projectId, endDate) {
            $log.debug('register end for ' + projectId + ' project at ' + endDate);
            var now = Date.now(),
                dif = endDate - now,
                diffSeconds = dif - 1000;


            $timeout(function(){
                endModal(projectId);
            }, diffSeconds);
        };

        var registerStartHandler = function(projectId, startDate) {
            $log.debug('register start for ' + projectId + ' project at ' + startDate);
            var now = Date.now(),
                diff = startDate - now,
                diffSeconds = diff - 1000;

            $timeout(function(){
                startModal(projectId);
            }, diffSeconds);
        };

        var endBidderProject =  function(id){
            var deferred = $q.defer();
            projectSvc.unjoin(id).then(function(res) {
                removeProject(id);
                //  this.removeAuctions(res.data.auctionIds);
                //modalSvc.display(res)
                deferred.resolve(res);

            }, function(res) {
                deferred.reject(res);
            });
            return deferred.promise;

        };

        var setProjects = function(p) {
            angular.forEach(p, addProject);
        };

        var addProject = function(projectDto) {
            var dto = angular.copy(projectDto);
            processProject(dto);
            svc.projects.push(dto);

        };

        var anyProjectActive = function(){
            var any = false;
            angular.forEach(svc.projects, function(value, key) {
                if(value.state === 3) {
                    any = true;
                }
                if(any)
                    return;
            });

            return any;
        }

        var processProject = function(project) {
            if(project.state === 2 || project.state === 3) {

                var now = Date.now(),
                    startDate = moment(project.start),
                    endDate = moment(project.end),
                    start = startDate.toDate() - now,
                    startSeconds = start - 1000,
                    end = endDate.toDate() - now,
                    endSeconds = end - 1000;

                if(startSeconds> 0) {
                    registerStartHandler(project.id, startDate.toDate());
                }
                if(endSeconds > 0) {
                    registerEndHandler(project.id, endDate.toDate());
                }
            }

            transformProject(project);

            if(svc.wsEnabled) {
                registerBidHandler(project.id);
            }
        };

        var transformProject = function(dto) {
            dto.newBid = false;
            if(!_.isUndefined(dto['start']['date'])) {
                dto.startDisplay = moment(dto.start).format("HH:mm:ss");
            }
            if(!_.isUndefined(dto['end']['date'])) {
                dto.endDisplay = moment(dto.end).format("HH:mm:ss");
            }
        };




        return {
            /*
             * If the auction has the new bid state, clears it
             */
            readProject: function(id) {
                angular.forEach(svc.auctions, function(value, key) {
                    if (value.projectId === id && value.newBid === true) {
                        svc.auctions[key].newBid = false;
                    }
                });

            },
            projects: svc.projects,
            getAuctions: function() {
                return svc.auctions;
            },
            getProjects: function() {
                return svc.projects;
            },
            addProject: addProject,
            addAuction: function(auctionDto) {
                return addAuction(auctionDto);
            },
            addAuctions: function(auctions) {
                angular.forEach(auctions, function(value, key) {
                    addAuction(value);
                });
            },
            endBidderProject: endBidderProject,
            removeProject: function(id) {
                return removeProject(id);
            },
            setProjects: setProjects,
            setAuctions: function(l) {
                // Subscribe to ws channels
                angular.forEach(l, function(obj) {
                    var auction = angular.copy(obj);
                    auction.newBid = false;
                    auction.bidState = AUCTION_FOLLOW_STATE.pristine;
                    this.push(obj);

                }, svc.auctions);
            },
            removeAuctions: function(ids) {
                var res =  removeAuctions(ids);
                $rootScope.userRank.auctionsDone = $rootScope.userRank.auctionsDone - ids.length;
                return res;
            },
            add: function(auctionId) {
                jobSvc.get(auctionId).then(function(res) {

                }, function(res) {

                });
            },
            /**
             * Remove the watchin on a auction, not the auction itself
             *
             * @param {type} auctionId
             * @returns {undefined}
             */
            unfollowAuction: function(auctionId) {
                jobSvc.removeFollowing($rootScope.userId, auctionId).then(function(res) {
                    removeAuctions([auctionId]);
                });
            },
            bid: function(auctionId, ammount) {

            },
            setCurrentView: function(id) {
                $log.info('current project view: ' + id);
                svc.currentProjectView = id;

                /*
                 * Clear new bids flags for project and his auctions */
                angular.forEach(svc.projects, function(project, key) {
                    if (project.id === id) {
                        svc.projects[key].newBid = false;
                        $log.info('project ' + id + ' new bid set to false');
                    }
                });
                angular.forEach(svc.auctions, function(auction, key) {
                    $log.info(JSON.stringify(auction));
                    if (auction.groupId === id) {
                        svc.auctions[key].newBid = false;
                        $log.info('auction ' + auction.id + ' newbid set to false');
                    }
                });
            },
            /**
             * Remove the current view
             *
             * @returns {undefined}
             */
            exitCurrentView: function() {
                $log.info('exit current view');
                svc.currentProjectView = null;
            },
            /**
             * Indicates if the user is currently viewing this project
             * @param {type} id
             * @returns {unresolved}
             */
            isCurrentView: function(id) {
                return isCurrentView(id);
            },
            /**
             * Indicates if a project is already being followed by the user
             * @param {string} id project id
             */
            isFollowing: function(id) {
                var f = false; // flag aux
                angular.forEach(svc.projects, function(value, key) {
                    if (value.id === id) {
                        f = true;
                        return false;
                    }
                });
                return f;
            },
            isFollowingAuction: function(id) {
                var f = false;
                angular.forEach(svc.auctions, function(value, key) {
                    if (value.id === id) {
                        f = true;
                        return false;
                    }
                });
                return f;
            },
            enableWs: function(){
                enableWs();
            },
            anyProjectActive: anyProjectActive,
            enableWsIfActive: function(){
                if(svc.projects.length == 0 || !anyProjectActive())
                    return;

                enableWs(true);
            },
            disableWs: function(){
                disableWs();
            }
        };
    };
    angular
        .module('fifi')
        .factory('auctionFollowSvc', ['$pusher', 'jobSvc', '$q', 'modalSvc', '$rootScope', '$log', 'fifiConfig', 'AUCTION_FOLLOW_STATE', '$timeout', '$modal', 'projectSvc', 'biddyConfiguration', svcFn]);
})();