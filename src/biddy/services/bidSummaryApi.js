(function(){
	var svc = function($http, $q){

		this.get = function(id) {
			var deferred = $q.defer(),
			successFn = function(res) {
				deferred.resolve(res.data);
			},
			errorFn = function(res) {
				deferred.reject(res);
			},
			httpObj = {
				url: '/api/bid/summary/' + id,
				method: 'GET'
			};

			$http(httpObj)
				.then(successFn, errorFn);

			return deferred.promise;
		};


		this.find = function(projectId, model) {

			var deferred = $q.defer(),
				successFn = function(res) {
					deferred.resolve(res.data);
				},
				errorFn = function(res) {
					deferred.reject(res);
				},
				httpObj = {
					url: '/api/project/' + projectId + '/summary',
					method: 'GET',
					params: model
				};

			$http(httpObj)
				.then(successFn, errorFn);

			return deferred.promise;
		};

		this.findByUser = function(userId, model) {
				var deferred = $q.defer(),
				successFn = function(res) {
					deferred.resolve(res.data);
				},
				errorFn = function(res) {
					deferred.reject(res);
				},
				httpObj = {
					url: '/api/bidder/summary/' + userId,
					method: 'GET',
					params: model
				};

			$http(httpObj)
				.then(successFn, errorFn);

			return deferred.promise;
		};

		this.postReputation = function(bidId, reputation) {
			var model = {
					id: bidId,
					reputation: reputation
				},
				deferred = $q.defer(),
				successFn = function(res) {
					deferred.resolve(res.data);
				},
				errorFn = function(res) {
					deferred.reject(res);
				},
				httpObj = {
					url: '/api/bid/reputation',
					method: 'POST',
					data: model
				};
				
			$http(httpObj)
				.then(successFn, errorFn);

			return deferred.promise;
		}
	};

	angular
		.module('fifi')
		.service('bidSummaryApi', ['$http', '$q', svc]);
})();