/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


angular.module('fifi').
        factory('projectSvc', ['Restangular', '$q', '$http', function(Restangular, $q, $http) {

        return {

            get: function(projectId, retrieveAuctions) {
                var deferred = $q.defer(),
                    successFn = function(res) {
                        deferred.resolve(res.data);
                    },
                    errorFn = function(res) {
                        deferred.reject(res);
                    },
                    httpObj = {
                        url: '/api/project/' + projectId,
                        method: 'GET'
                    };

                    if(!_.isUndefined(retrieveAuctions) && retrieveAuctions) {
                        httpObj.url = httpObj.url + '?auctions=true'
                    }

                    $http(httpObj)
                        .then(successFn, errorFn);

                    return deferred.promise;
            },
        /**
         * @ngdoc method
         * @name Find
         * @description
         * 
         */
        sendDiscount: function(projectId, discount, description) {
            var model = {
                    projectId: projectId,
                    discount: discount,
                    description: description
                },
                deferred = $q.defer(),
                successFn = function(res) {
                    deferred.resolve(res.data);
                },
                errorFn = function(res) {
                    deferred.reject(res);
                },
                httpObj = {
                    url: '/api/project/discount',
                    method: 'POST',
                    data: model
                };

                $http(httpObj)
                    .then(successFn, errorFn);
                    
                return deferred.promise;
        },
        find: function(skip, take, queryModel) {
        var deferred = $q.defer(),
                req = angular.copy(queryModel || {});
                req.skip = skip;
                req.take = take;
                $http({
                method: 'GET',
                        url: '/api/project',
                        params: req
                }).
                success(function(res) {
                deferred.resolve(res);
                }).
                error(function(res) {
                deferred.reject(res);
                });
                return deferred.promise;
        },
                create: function(model) {
                var defer = $q.defer(),
                        //jobCache = $angularCacheFactory.get('jobCache'),
                        req = angular.copy(model);
                        Restangular.
                        all('project').
                        customPOST(req, '').
                        then(function(res) {
                        //jobCache.put(res.id, res);
                        return defer.resolve(res);
                        }, function(res) {
                        return defer.reject(res);
                        });
                        return defer.promise;
                },
                updateInfo: function(projectId, model) {
                var deferred = $q.defer(),
                        req = angular.copy(model);
                        Restangular.
                        all('project').
                        one(projectId).
                        customPOST(req, 'info').
                        then(function(res) {
                        deferred.resolve(res.originalResponse);
                        }, function(res) {
                        deferred.reject(res);
                        });
                        return deferred.promise;
                },
                remove: function(id) {
                return Restangular.all('project').
                        one(id).
                        remove();
                },
                unjoinAuctions: function(ids) {
                var deferred = $q.defer();
                        $http.post('/api/project/' + ids + '/unjoin', {ids: ids}).
                        success(function(res) {
                        deferred.resolve(res);
                        }).
                        error(function(res) {
                        deferred.reject(res);
                        });
                        return deferred.promise;
                },
        /**
         * @name Join
         * 
         * @description Join a active project
         * @param {mongo_id} id
         * @returns promisse
         */
        join: function(id, auctionIds) {
        var defer = $q.defer();
                Restangular.all('project').one(id).
                customPOST({
                ids: auctionIds
                }, 'like').
                then(function(res) {
                defer.resolve(res.originalResponse);
                }, function(res) {
                defer.reject(res);
                });
                return defer.promise;
        },
                unjoin: function(id) {

                    return $http({
                        method: 'DELETE',
                        url: '/api/project/' + id + '/like'
                    });

                },
                /**
                 * @description Get a list of active projects the user is joinded
                 * @returns promisse
                 */
                getJoin: function() {
                var deferred = $q.defer();
                        Restangular.all('project').
                        customGET('like', {}).
                        then(function(res) {
                        deferred.resolve(res);
                        }, function(res) {
                        deferred.reject(res);
                        });
                        return deferred.promise;
                        return deferred.promise;
                },
                /**
                 * @ngdoc function
                 * @name Publish
                 * 
                 * @description
                 * Publish a project and enqueue all auctions
                 * 
                 * @param mongo_id id
                 */
                publish: function(id, start, end){
                var deferred = $q.defer(),
                    request = {
                        start: start,
                        end: end
                    };


                        Restangular.all('project').
                        one(id).
                        customPOST(request, 'publish').
                        then(function(res) {
                        deferred.resolve(res.originalResponse);
                        }, function(res) {
                        deferred.reject(res);
                        });
                        return deferred.promise;
                },
                /**
                 * @ngdoc function
                 * @name Activate
                 * 
                 * @description
                 * Activate a project
                 * @param mongo_id id
                 */
                activate: function(id) {
                var deferred = $q.defer();
                        Restangular.all('project').one(id).
                        customPOST({}, 'activate').
                        then(function(res) {
                        deferred.resolve(res.originalResponse);
                        }, function(res) {
                        deferred.reject(res);
                        });
                        return deferred.promise;
                },
                createController: function() {
                var defer = $q.defer();
                        Restangular.
                        one('project.json/create').
                        get().
                        then(function(res) {
                        defer.resolve(res);
                        }, function(res) {
                        defer.reject(res);
                        });
                        return defer.promise;
                },
                editController: function(projectId) {
                var defer = $q.defer();
                        Restangular.
                        one('project.json').
                        one(projectId).
                        one('edit').
                        get().
                        then(function(res) {
                        defer.resolve(res);
                        }, function(res) {
                        defer.reject(res);
                        });
                        return defer.promise;
                },
                viewController: function(projectId) {
                var defer = $q.defer();
                        Restangular.
                        one('project.json').
                        one(projectId).
                        one('view').
                        get().
                        then(function(res) {
                        defer.resolve(res.originalResponse);
                        }, function(res) {
                        defer.reject(res);
                        });
                        return defer.promise;
                },
            saveFile: function(projectId, fileId, model) {
                var req = angular.copy(model),
                    deferred = $q.defer(),
                    successFn = function(res) {
                        deferred.resolve(res.data);
                    },
                    errorFn = function(res) {
                        deferred.reject(res);
                    },
                    httpObj = {
                        method: 'POST',
                        url: '/api/project/' + projectId + '/file/' + fileId,
                        data: model
                    };

                $http(httpObj)
                    .then(successFn, errorFn);

                return deferred.promise;
            },
                removeFile: function(projectId, fileId) {
                var deferred = $q.defer();
                        $http({
                        method: 'DELETE',
                                url: '/api/project/' + projectId + '/file/' + fileId
                        }).success(function(res) {
                deferred.resolve(res);
                }).error(function(res) {
                deferred.reject(res);
                });
                        return deferred.promise;
                },
                getFiles: function(projectId, skip, take) {
                    return $http.get('/api/project/' + projectId + '/file');
                }
        };
        }]);
