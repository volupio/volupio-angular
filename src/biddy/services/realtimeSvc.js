angular.module('fifi').
        directive('fifiRealtime', ['realtimeSvc', '$log', function(realtimeSvc, $log) {
                return {
                    link: function(scope, elem, attrs) {
                        realtimeSvc.onopen(function() {
                            realtimeSvc.call('init', {}).then(function(res) {
                                $log.info('directive success');
                            }, function(res) {
                                $log.info('directive.error');
                            });
                        });
                    }
                }
            }]).
        provider('piConfig', [function(){
                this.wsPortConfig = 8080;
                this.$get = function(){
                    return {
                        wsPort: wsPortConfig
                    };
                };
        }]).
            /** 
             * Handlers
             * */
        factory('realtimeSvc', ['$q', '$log', '$rootScope', '$location', 'piConfigProvider',
            function($q, $log, $rootScope, $location, piConfigProvider) {
                var callbacks = {},
                        handlers = {},
                        currentId = 0,
                        port = piConfigProvider.wsPort,
                        hostname = window.location.hostname,
                        url = $location.protocol() === 'https'
                        ? 'wws://' + hostname
                        : 'ws://' + hostname;

                if (_.isNumber(port)) {
                    url = url + ':' + port;
                }

                    var wsocket = new WebSocket(url);

                    wsocket.onopen = function() {
                        svc.connected = true;
                        if (svc.handlers.onopen) {
                            $rootScope.$apply(function() {
                                svc.handlers.onopen.apply(wsocket);
                            });
                        }
                    };

                    wsocket.onclose = function() {
                        svc.connected = false;
                        // Create a new socket
                        setTimeout(function() {
                            wsocket = create();
                        }, 3000);
                    };

                    wsocket.onerror = function(res) {
                        $log.info('Websocket error detected: ' + JSON.stringify(res));
                    };

                    wsocket.onmessage = function(e) {
                        var res = JSON.parse(e.data);
                        $log.info("Websocket message received: " + res);

                        $rootScope.$apply(callbacks[res.id].cb.resolve(res.result));
                        delete callbacks[res.id];
                    };

                var svc = {
                    handlers: {},
                    connected: false,
                    onopen: function(callback) {
                        this.handlers.onopen = callback;
                    },
                    onclose: function(callback) {
                        this.handlers.onclose = callback;
                    },
                    call: function(action, params) {

                        if (!this.connected) {
                            setTimeout(function() {
                            }, 3000);
                        }
                        var deferred = $q.defer();
                        var request = {
                            "jsonrpc": "2.0",
                            "method": action,
                            "params": params
                        };

                        currentId += 1;
                        request.id = currentId;
                        callbacks[currentId] = {
                            time: new Date(),
                            cb: deferred
                        };

                        wsocket.send(JSON.stringify(request));

                        return deferred.promise;
                    }
                };

                return svc;
            }]);