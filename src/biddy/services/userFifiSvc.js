/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('fifi').
        factory('userFifiSvc', ['Restangular', '$q', '$http', function(Restangular, $q, $http) {
                return {
                    getEditCtrl: function(id) {
                        var deferred = $q.defer();
                        Restangular.all('user').one(id).one('edit').get().
                                then(function(res) {
                                    deferred.resolve(res);
                                }, function(res) {
                                    deferred.reject(res);
                                });
                        
                        return deferred.promise;
                    },
                    getProfile: function(id) {
                        var deferred = $q.defer();
                        Restangular.all('user').
                                one(id).
                                one('profile').
                                get().
                                then(function(res) {
                                    deferred.resolve(res.originalResponse);
                                }, function(res) {
                                    deferred.reject(res);
                                });
                        return deferred.promise;
                    },
                    getProfileCtrl: function(id){
                        var deferred = $q.defer();
                        
                        Restangular.one('account/profile/assign.json?id=' + id).get().
                                then(function(res) {
                                   deferred.resolve(res.originalResponse); 
                                }, function(res) {
                                    deferred.reject(res);
                                });
                        
                        return deferred.promise;
                    },
                    find: function(model) {
                        var deferred = $q.defer();
                        
                        $http({
                           method: 'GET',
                           url: '/api/v2/user',
                           params: model
                        }).success(function(res) {
                            deferred.resolve(res);
                        }).error(function(res) {
                            deferred.reject(res);
                        });
                        
                        return deferred.promise;
                    }
                };
            }]);