angular.module('fifi').
        service('bidderSvc', ['$q', 'Restangular', 'auctionFollowSvc', '$rootScope', '$http',
            function($q, Restangular, auctionFollowSvc, $rootScope, $http) {
                var bidderSvc = {};
                
                /**
                 * Initialize Bidder 
                 * 
                 * @param object model
                 */
                bidderSvc.init = function(model) {
                    $rootScope.isBidder = true;
                    $rootScope.userRank = {
                                    profile: 1,
                                    auctionsDone: model.rank.auctionsDone,
                                    auctionsWinned: model.rank.auctionsWinned,
                                    reputation: model.rank.reputation,
                                    technicalTeam: 0,
                                    level: 0,
                                    generalExperience: 0
                                };
                                
                    $rootScope.followSvc.setAuctions(model.auctionsFollowing);
                    $rootScope.followSvc.setProjects(model.projectsFollowing);
                    $rootScope.followSvc.enableWsIfActive();
                    
                };

                bidderSvc.confirmAdmin = function(id) {
                    var deferred = $q.defer();

                    Restangular.all('bidder').one(id).
                            customPOST({}, 'confirmadmin').then(function(res) {
                        deferred.resolve(res.originalResponse);
                    }, function(res) {
                        deferred.reject(res);
                    });

                    return deferred.promise;
                };


                bidderSvc.getCvCtrl = function(userId){
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        },
                        httpConfig = {
                            url: '/bidders/cv.json/' + userId,
                            method: 'GET'
                        };

                        $http(httpConfig)
                            .then(successFn, errorFn);

                        return deferred.promise;
                };

                bidderSvc.saveCvInfo = function(userId, cvDescription) {
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        },
                        httpObj = {
                            url: '/api/bidder/' + userId + '/cv',
                            method: 'POST',
                            data: {
                                cvDescription: cvDescription
                            }
                        };

                    $http(httpObj)
                        .then(successFn, errorFn);

                    return deferred.promise;
                };

                bidderSvc.saveFile = function(userId, fileId, model) {
                    var req = angular.copy(model),
                        deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        },
                        httpObj = {
                            method: 'POST',
                            url: '/api/bidder/cv/' + userId + '/' + fileId,
                            data: model
                        };

                    $http(httpObj)
                        .then(successFn, errorFn);

                    return deferred.promise;
                };
                
                bidderSvc.findCtrl = function(model) {
                    return $http.get('/bidders/list.json');
                };
                
                bidderSvc.viewCtrl = function(id) {
                    return $http.get('/bidders/view.json/' + id);
                };
                
                bidderSvc.getCard = function(id) {
                    var deferred = $q.defer(),
                        successFn = function(res) {

                        },
                        errorFn = function(res) {

                        };
                    $http({
                        method: 'GET',
                        url: '/bidder/card.json/' + id
                    })
                    .then(successFn, errorFn);
                    return deferred.promise;
                };
                
                bidderSvc.register = function(){
                    
                };

                /**
                 * Get a user card
                 * 
                 * @param {type} userId
                 * @returns {undefined}
                 */
                bidderSvc.getUserCard = function(userId) {
                    var deferred = $q.defer();

                    Restangular.all('bidder').one(userId).customGET({}, 'card').then(function(res) {
                        var response = {
                            name: res.displayName,
                            uri: res.uri
                        };
                        deferred.resolve(response);
                    }, function(res) {
                        deferred.reject(res);
                    });

                    return deferred.promise;
                };
                
                bidderSvc.getCvFile = function() {
                    
                };
                
                bidderSvc.getMyCategoriesCtrl = function(id){
                    var url = '/bidder/my-categories.json';
                    if(!_.isUndefined(id)) {
                        url = url + '?id=' + id;
                    }
                    return $http.get(url);
                };
                
                bidderSvc.addCategory = function(categoryId, userId) {
                    var model = {categories: [categoryId]},
                        url = '/api/bidder/category';

                    if(!_.isUndefined(userId) && !_.isNull(userId)) {
                        url = url + '?id=' + userId;
                    }
                    return $http.post(url, model);
                };
                
                bidderSvc.delCategory = function(id, userId) {
                    var url = '/api/bidder/category/' + id;
                    if(!_.isUndefined(userId)) {
                        url = url + '?id=' + userId;
                    }
                    return $http({
                        method: 'DELETE',
                        url: url
                    });
                };

                bidderSvc.getRankboardCtrl = function(id){
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        };

                    $http({
                        method: 'GET',
                        url: '/bidders/rank.json'
                    })
                        .then(successFn, errorFn);
                    return deferred.promise;
                };

                bidderSvc.getRankboard = function(){
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        },
                        httpConfig = {
                            method: 'GET',
                            url: '/api/bidder/rank'
                        };

                    $http(httpConfig)
                        .then(successFn, errorFn);

                    return deferred.promise;
                };

                bidderSvc.setReputation = function(userId, reputation) {
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        },
                        model = {
                            reputation: reputation
                        },
                        httpObj = {
                            method: 'POST',
                            url: '/api/bidder/reputation/' + userId,
                            data: model
                        };

                    $http(httpObj)
                        .then(successFn, errorFn);

                    return deferred.promise;
                };

                bidderSvc.findRank = function(model) {
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        };

                    $http({
                        method: 'GET',
                        url: '/bidders/rank.json',
                        params: model
                    })
                    .then(successFn, errorFn);

                    return deferred.promise;
                };

                bidderSvc.getRank = function(id) {
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        };

                    $http({
                        method: 'GET',
                        url: '/api/bidder/' + id + '/rank'
                    })
                        .then(successFn, errorFn);

                    return deferred.promise;
                };


               /**
                bidderSvc.setDiscount =function (projectId, discount) {
                    var deferred = $q.defer(),
                        successFn = function(res) {
                            deferred.resolve(res.data);
                        },
                        errorFn = function(res) {
                            deferred.reject(res);
                        },
                        request = {
                            projetId: projectId,
                            discountId: discount
                        },
                        httpObj = {
                            url: '/api/bid/discount',
                            method: 'POST',
                            data: request
                        };

                    $http(httpObj)
                        .then(successFn, errorFn);

                    return deferred.promise;
                };*/


                return bidderSvc;
            }]);