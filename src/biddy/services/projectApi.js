angular.module('fifi').
        service('projectApi', ['$http', '$q', function($http, $q) {
                this.getController = function(){
                    return $http.get('/projects.json');
                };
        }]);