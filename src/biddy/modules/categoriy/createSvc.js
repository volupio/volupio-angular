(function(){
    var svcFn = function(api, $q, modalSvc, $state) {

        var create = function(name, content, parentId) {
            var deferred = $q.defer(),
                request = {
                    name: name,
                    content: content
                };
            if(!_.isEmpty(parentId)) {
                request.parent = parentId;
            }
            api.post(request).then(function(res) {
                deferred.resolve(res.data);
            }, function(res) {
                deferred.reject(res);
            });

            return deferred.promise;
        };
        
        var getModel = function(skip, take)  {
            var deferred = $q.defer();
            api.get().then(function(res) {
                deferred.resolve({
                    categories: res.data.results
                }, function(res) {
                    deferred.reject(res.data);
                });
            });
            return deferred.promise;
        };
        return {
            create: create,
            getModel: getModel
        };
    };

    svcFn.$inject = ['auctionCategoryApi', '$q', 'modalSvc', '$state'];
    
    angular
        .module('fifi')
        .service('auctionCategoryCreateSvc', svcFn);
})();