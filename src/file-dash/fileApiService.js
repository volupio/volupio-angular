(function(){
    var fileApiService = function($http, $q) {

        var svc = function(restPath, entityId) {
            return {
                getFiles: function(){
                    var deferred = $q.defer(),
                        success = function(res) {
                            deferred.resolve(res.data);
                        },
                        error = function(res) {
                            deferred.reject(res);
                        },
                        httpObj = {
                            method: 'GET',
                            url: '/api/' + restPath + '/' + entityId + '/file'
                        };
                    $http(httpObj)
                        .then(success, error);

                    return deferred.promise;
                },
                saveFile: function(id, name, content) {
                    var deferred = $q.defer(),
                        success = function(res) {
                            deferred.resolve(res.data);
                        },
                        error = function(res) {
                            deferred.reject(res);
                        },
                        httpObj = {
                            method: 'POST',
                            url: '/api/' + restPath + '/' + entityId + '/file/' + id,
                            data: {
                                id: id,
                                name: name,
                                content: content
                            }
                        };
                    $http(httpObj)
                        .then(success, error);

                    return deferred.promise;
                },
                removeFile: function(id) {

                },
                getFile: function(fileId) {
                    var deferred = $q.defer(),
                        success = function(res) {
                            deferred.resolve(res.data);
                        },
                        error = function(res) {
                            deferred.reject(res);
                        },
                        httpObj = {
                            method: 'GET',
                            url: '/api/file/' + fileId
                        };
                    $http(httpObj)
                        .then(success, error);

                    return deferred.promise;
                }
            }
        };

        return svc;
    };

    fileApiService.$inject = ['$http', '$q'];

    angular
        .module('volupio')
        .service('fileDashService', fileApiService);
})();