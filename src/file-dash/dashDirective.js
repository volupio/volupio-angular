(function(){
    var attachsDashDirective = function(attachmentsDash){
        return {
            scope: {
                'entityId': '@',
                'allowSelection': '=',
                'entityRest': '@',
                'fileDashOnClose': '=',
                'fileDashOnClose': '='
            },
            link: function(scope, elem, atts) {

                // Allow selection
                
                elem.bind('click', function(){
                    var obj = {
                        entityId: scope.entityId,
                        restPath: scope.entityRest
                    };
                    if(scope.allowSelection) {
                        obj.allowSelection = true;
                    }
                    var promise = attachmentsDash.open(obj);
                    
                    promise
                        .then(function(res) {
                            scope.fileDashOnClose(res);
                        }, function(res) {
                            scope.fileDashOnClose(res);
                            fn(res);
                        });
                });
            }
        }
    };

    attachsDashDirective.$inject = ['attachmentsDash'];

    angular
        .module('volupio')
        .directive('fileDash', attachsDashDirective);
})();
/*
,
            controller: function($scope, attachmentsDash) {
                $scope.open = function(){
                    var promise = attachmentsDash.open($scope.entityId, $scope.entityRest);
                    promise
                        .then(function(res) {
                            var fn = $scope.fileDashOnClose();
                            if(!_.isUndefined(fn)) {
                                fn(res);
                            }
                        })
                }


                
            }*/