(function(){

    var attachmentsDashProvider = function(){

        var getFn = function ($modal, $upload, modalSvc, fileDashService, $q) {
            return {
                open: function(idOrConfiguration, restPath) {
                    var allowSelection = false,
                        entityId;

                    if(typeof(idOrConfiguration) === "object" && idOrConfiguration !== null) {
                        restPath = idOrConfiguration.restPath,
                        entityId = idOrConfiguration.entityId;
                        allowSelection = idOrConfiguration.allowSelection;
                    }
                    var svc = new fileDashService(restPath, entityId);
                    var deferred = $q.defer();

                    var instance = $modal.open({
                        controller: ['$scope', '$modalInstance', function($scope, $modalInstance) {
                            $scope.editView = false;
                            svc.getFiles().then(function(res) {
                                $scope.files = res.files;
                            });


                            $scope.edit = function(id) {
                                $scope.file = {
                                    id: id
                                };
                                svc.getFile(id).then(function(res) {
                                    $scope.file.name = res.result.name;
                                    $scope.file.content = res.result.content;
                                    $scope.editView = true;
                                });
                            };

                            $scope.save = function(){
                                svc.saveFile($scope.file.id, $scope.file.name, $scope.file.content)
                                    .then(function(res) {
                                        angular.forEach($scope.files, function(value, key) {
                                            if(value.id == $scope.file.id) {
                                                value.name = $scope.file.name;
                                                value.content = $scope.file.content;
                                            }
                                        })
                                        $scope.editView = false;
                                    });
                            };

                            $scope.goDashHome = function(){
                                $scope.editView = false;
                            }

                            $scope.close = function(){
                                $modalInstance.close({
                                    files: $scope.files
                                });
                            };

                            $scope.form = {};

                            $scope.onFileSelect = function($files) {

                                $scope.selectedFiles = [];
                                $scope.progress = [];
                                if ($scope.upload && $scope.upload.length > 0) {
                                    for (var i = 0; i < $scope.upload.length; i++) {
                                        if ($scope.upload[i] != null) {
                                            $scope.upload[i].abort();
                                        }
                                    }
                                }
                                $scope.uploadResult;
                                $scope.selectedFiles = $files;
                                $scope.progress = -1;

                                //$files: an array of files selected, each file has name, size, and type.

                                var files = $files;
                                var names = [];

                                _.forEach($files, function(file) {
                                    names.push(file.name);
                                });
                                $scope.upload = $upload.upload({
                                    url: '/api/' + restPath + '/' + entityId + '/file',
                                    file: files,
                                    fileFormDataName: names
                                }).error(function(data, status, headers, config) {
                                    if(status === 413)  {
                                        modalSvc.display({message: '<p>A imagem que está a anexar é muito grande.</p><p>Deverá fazer o upload de imagens inferiores a 10 Mb.</p>'});
                                    } else {
                                        modalSvc.internalError();
                                    }
                                }).success(function (data, status, headers, config) {
                                    angular.forEach(data.files, function(value) {
                                        $scope.files.push(value);
                                    });
                                });

                            };
                        }],
                        templateUrl: '/web/file/dash.html',
                        size: 'lg'
                    }).result;

                    return instance;

                    instance.then(function(res) {
                        deferred.resolve({
                                    files: $scope.files
                                });
                    }, function(res) {
                        deferred.reject(res);
                    });

                    return deferred.promise;
                }
            };
        };

        getFn.$inject = ['$modal', '$upload', 'modalSvc', 'fileDashService', '$q'];

        this.$get = getFn;
    };

    angular
        .module('volupio')
        .provider('attachmentsDash', attachmentsDashProvider);
})();