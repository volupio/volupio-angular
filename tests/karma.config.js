module.exports = function(config) {
    config.set({
        basePath: '../',
        frameworks: ['jasmine'],
        files: [
            'bower/angular/angular.js',
            'bower/angular-mocks/angular-mocks.js',
            'bower/jquery/dist/jquery.js',
            'bower/restangular/dist/restangular.js',
            'bower/angular-cache/dist/angular-cache.js',
            'bower/angular-cookies/angular-cookies.js',
            'bower/underscore/underscore.js',
            'src/module.js',
            'src/*.js',
            'src/**/*.js',
            'tests/*.js'
        ],
        exclude: [
        ],
        preprocessors: {
        },
        reporters: ['progress'],
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: false,
        browsers: ['Chrome'],
        plugins: [
            'karma-script-launcher',
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-phantomjs-launcher',
            'karma-jasmine',
            'watch'
        ],
        singleRun: false
    });
};
