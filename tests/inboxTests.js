'use strict';

describe('Tests for inbox component', function(){
	
	beforeEach(angular.mock.inject(function(){
		//angular.mock.module('volupio', 'volupio.inbox');
		$httpBackend.when("GET", '/api/inbox').respond([{id: '1', title'asd'}, {id: '2', title'asd'}, {id: '3', title'asd'}]);
        $controller("reportListController", {
            $scope: scope
        });
        $httpBackend.flush();
	}));
		
	afterEach(function() {
		_$httpBackend.verifyNoOutstandingExpectation();
		_$httpBackend.verifyNoOutstandingRequest();
	});

	it("should load inbox users list", function(){
		_inboxSvc.getConversations(0, 100).then(function(res) {
			expect(res.results.length).toBeGreaterThan(0);
		});
	});

});