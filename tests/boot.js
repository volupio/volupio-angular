/**
 * Boot file for tests
 *
 * Inject the main dependencies used in tests
 * Also ensure the http backend was success
 */
var $rootScope, $controller, $q, $httpBackend, appConfig, scope;
angular.mock.module('volupio', 'volupio.inbox');
// The inject function knows how to strip variables with underscores at begin and the end like _name_. This way the outer scope can have pretty names like $scope instead of _$scope or something else
beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$httpBackend_, _appConfig_) {
    $q = _$q_;
    $rootScope = _$rheootScope_;
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    appConfig = _appConfig_;
    scope = $rootScope.$new();
}));
 
afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
});