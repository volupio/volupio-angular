Volupio Angular
==============

## Installation
Use bower to install this modules. Because this is a private repo, you'll have to check it using SSH instead of HTTPS

bower install git@bitbucket.org:volupio-team/volupio-angular.git

## Tests
Tests are configured with karma