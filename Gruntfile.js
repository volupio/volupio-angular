module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
                concat: {
                    volupio: {
                        dest: 'dist/volupio.js',
                        src: [
                            'src/module.js',
                            'src/**/module.js',
                            'src/*.js',
                            'src/**/*.js',
                            '!src/biddy/**'
                        ]
                    },
                    biddy: {
                    dest: 'dist/volupio-biddy-angular.js',
                    src: [
                        'src/biddy/module.js',
                        'src/biddy/**/module.js',
                        'src/biddy/*.js',
                        'src/biddy/**/*.js',
                        'src/biddy/**/**/module.js',
                        'src/biddy/**/**/*.js'
                    ]
                }
                },
                karma: {
                development: {
                  configFile: 'tests/karma.config.js'
              },
            }
	});
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-karma');
	grunt.registerTask('default', ['concat']);
};